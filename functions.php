<?php
	/**
	 * Superboss functions and definitions
	 *
	 * @link https://developer.wordpress.org/themes/basics/theme-functions/
	 *
	 * @package Superboss
	 */

	$superboss_root = get_template_directory();


	/* Include all files in directory
	============================================================================= */

	function superboss_include_directory( $directory = false ) {
		global $superboss_root;

		$filepath = $superboss_root . '/' . $directory;

		if ( is_dir( $filepath ) ) {
			foreach ( glob( $filepath . '/*.php' ) as $file ) {
				require_once $file;
			}
		} else {
			trigger_error( esc_html( sprintf( 'Error locating directory %s for inclusion', $filepath ) ), E_USER_ERROR );
		}
	}

	/* Include directories
	============================================================================= */

	/* Functions */
	superboss_include_directory( 'functions' );

	/* Custom Post Types & Fields */
	superboss_include_directory( 'custom' );
