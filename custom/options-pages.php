<?php

	/* Add Custom Settings Pages
	============================================================================= */

	$theme_settings_args = array(
		'page_title' 	=> 'Theme Settings',
		'menu_title' 	=> 'Theme Settings',
		'menu_slug' 	=> 'theme-settings',
		'capability' 	=> 'edit_posts',
		'icon_url' => 'dashicons-admin-generic',
		'redirect' 	=> false
	);

	$footer_settings_args = array(
		'page_title' 	=> 'Footer Settings',
		'menu_title' 	=> 'Footer Settings',
		'menu_slug' 	=> 'footer-settings',
		'capability' 	=> 'edit_posts',
		'icon_url' => 'dashicons-editor-insertmore',
		'redirect' 	=> false
	);

	$content_404_args = array(
		'page_title' 	=> '404 Page Content',
		'menu_title' 	=> '404 Content',
		'menu_slug' 	=> 'content-404',
		'capability' 	=> 'edit_posts',
		'icon_url' => 'dashicons-warning',
		'redirect' 	=> false
	);

	if ( function_exists( 'acf_add_options_page' ) ) {
		$navigation_option_page = acf_add_options_page( $theme_settings_args );
		$navigation_option_page = acf_add_options_page( $footer_settings_args );
		$navigation_option_page = acf_add_options_page( $content_404_args );
	}
