<?php
	/**
	 * The template for displaying the footer
	 *
	 * Contains the closing of the #content div and all content after.
	 *
	 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
	 *
	 * @package Superboss
	 */
?>
			</main><!-- end .page -->

			<?php get_template_part( 'template-parts/footer' ); ?>
		</div><!-- end .page-wrapper -->
		<?php
			get_template_part( 'template-parts/navigation', 'mobile' );
			wp_footer();
		?>
	</body>
</html>
