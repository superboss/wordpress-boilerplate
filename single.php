<?php
	/**
	 * The template for displaying all single posts
	 *
	 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
	 *
	 * @package Superboss
	 */

	get_header();

	while ( have_posts() ) : the_post();
		get_template_part( 'template-parts/page', 'header' );
		get_template_part( 'template-parts/content', 'page' );
	endwhile;

	// get_sidebar();
	get_footer();
