# [Superboss WordPress Theme](https://bitbucket.org/matthisamoto/wordpress-boilerplate/)

This is the base WordPress Theme for Superboss.

Built on the Underscores starter theme and using Gulp to manage assets, there are basic HTML layouts set up to save time and effort in starting up a project.

## Work Flow

* Gulp
* Advanced Custom Fields + Custom Post Type UI
* Modernizr
* Reptile by Superboss: Assets Pipeline