<?php
	/**
	 * The template for displaying search results pages
	 *
	 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
	 *
	 * @package Superboss
	 */

	global $wp_query;

	$pagination = superboss_posts_pagination( $wp_query );

	get_header();

	get_template_part( 'template-parts/page-header', 'search' );
?>
<section class="search-results margins-standard">
	<?php
		if ( get_search_query() ) :
			if ( have_posts() ) :
	?>
	<div class="container">
		<?php
				while ( have_posts() ) : the_post();
					get_template_part( 'template-parts/content', 'search' );
				endwhile;
		?>
	</div>
	<?php if ( $pagination ) : ?>
		<div class="container">
			<footer class="content-pagination">
				<?php echo $pagination; ?>
			</footer>
		</div>
	<?php
				endif;
			else :
				get_template_part( 'template-parts/content', 'none' );
			endif;
		endif;
	?>
</section>
<?php

	get_footer();
