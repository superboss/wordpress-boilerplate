<?php
	/**
	 * The sidebar containing the main widget area
	 *
	 * @package Superboss
	 */

	$subnavigation_args = array(
		'post_type' => $post->post_type,
		'title_li' => '',
		'depth' => 3,
		'echo' => 0,
		'order' => 'ASC',
		'orderby' => 'menu_order',
	);

	$sibling_args = array_merge(
		array(
			'include' => $post->post_parent,
		),
		$subnavigation_args
	);

	$child_args = array_merge(
		array(
			'child_of' => $post->post_parent
		),
		$subnavigation_args
	);

	$siblings = wp_list_pages( $sibling_args );
	
	if ( $post->post_parent !== 0 ) :
		$children = wp_list_pages( $child_args );
	endif;
?>
<aside class="sidebar sidebar-main">
	<nav class="subnavigation">
		<ul>
			<?php
				if ( isset( $siblings ) ) : echo $siblings; endif;
				if ( isset( $children ) ) : echo $children; endif;
			?>
		</ul>
	</nav>
</aside>
