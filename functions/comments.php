<?php
	/**
	 * DOM Class Functions
	 *
	 * Add functions related to front-end system classes
	 *
	 * @package Superboss
	 */


	/* Customize Comment Markup
	============================================================================= */

	function superboss_comments( $comment, $args, $depth ) {
		$GLOBALS['comment'] = $comment;
		$rating = get_field( 'comment_rating', $comment );
?>
<li <?php comment_class( 'review-item' ); ?> id="li-comment-<?php comment_ID() ?>">
	<?php if ( get_field( 'comment_title', $comment ) ) : ?>
	<span><?php the_field( 'comment_title', $comment ) ?></span>
	<?php endif; ?>
	<figure class="body-xs">
		<?php printf(__('%s'), get_comment_author_link()) ?>
		on
		<a class="comment-permalink" href="<?php echo htmlspecialchars ( get_comment_link( $comment->comment_ID ) ) ?>"><?php printf(__('%1$s'), get_comment_date(), get_comment_time()) ?></a>
	</figure>
	<div class="stars">
		<div class="stars-mask">
			<svg class="bg-mask" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 223.5 54.5"><path d="M76.2,140.2v54.6H299.8V140.2Zm212,47.5L274.8,178l-13.4,9.7,5.1-15.8-13.4-9.7-13.3,9.7,5.1,15.8L231.5,178l-13.4,9.7,5.1-15.8-13.4-9.7-13.3,9.7,5.1,15.8L188.2,178l-13.4,9.7,5.1-15.8-13.4-9.7-13.4,9.7,5.1,15.8L144.8,178l-13.4,9.7,5.1-15.8-13.3-9.7-13.4,9.7,5.1,15.8L101.5,178l-13.4,9.7,5.1-15.8-13.4-9.7H96.4l5.1-15.8,5.1,15.8h33.1l5.1-15.8,5.2,15.8h33l5.2-15.8,5.1,15.8h33.1l5.1-15.8,5.1,15.8h33.1l5.1-15.8,5.1,15.8h16.6l-13.4,9.7Z" transform="translate(-76.3 -140.3)"/></svg>
			<div class="bar rate-<?php echo $rating * 20; ?>"></div>
			<div class="base"></div>
		</div>
	</div>
	<?php
		if ($comment->comment_approved == '0') :
	?>
	<em><php _e('Your comment is awaiting moderation.') ?></em><br />
	<?php
		endif;
		
		comment_text();
	?>
	<!--
	<div class="reply">
		<?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
	</div>
	-->
	<a href="" class="body-xs">Report</a>
<?php
	}
