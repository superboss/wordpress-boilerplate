<?php
	/**
	 * WPSEO Breadcrumb Functions
	 *
	 * Functions related to Yoast WordPress SEO Breadcrumbs
	 *
	 * @package Superboss
	 */


	/* Remove Home from Breadcrumbs
	============================================================================= */

	function superboss_wpseo_remove_home_from_breadcrumb( $links ) {

		if ( $links[0]['url'] == home_url('/') ) {
			array_shift($links); 
		}

		return $links;
	} 

	// add_filter('wpseo_breadcrumb_links', 'superboss_wpseo_remove_home_from_breadcrumb', 10 );


	/* Remove Current Page from Breadcrumbs
	============================================================================= */

	function superboss_wpseo_remove_current_page_from_breadcrumb( $links ) {

		if ( count( $links ) == 1 ){
			$links = array();
			$links[] = array();
		} else {
			array_pop( $links );
			$links[] = array();
		}

		return $links;
	} 

	// add_filter('wpseo_breadcrumb_links', 'superboss_wpseo_remove_current_page_from_breadcrumb', 20 );
