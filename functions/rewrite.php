<?php
	/**
	 * Functions related to rewrite rules
	 *
	 * @package Superboss
	 */

	/* Rewrite URLs
	============================================================================= */

	function superboss_rewrite_urls ( $rules ) {

		add_rewrite_rule(
			'^[path]/([^/]*)/?',
			'index.php?pagename=[path]/$matches[1]',
			'top'
		);
	}

	// add_filter( 'init', 'superboss_rewrite_urls' );
