<?php
	/**
	 * Search
	 *
	 * Add functions related to search.
	 *
	 * @package Superboss
	 */

	/* Serve Search results through Search Page template
	============================================================================= */

	function superboss_change_search_url() {
		global $wp_rewrite;

		if ( is_search() && isset ( $_GET['s'] ) ) {
			$s         = sanitize_text_field( $_GET['s'] ); // or get_query_var( 's' )
			$location  = '/';
			$location .= trailingslashit( $wp_rewrite->search_base );
			$location .= ( ! empty ( $s ) ) ? user_trailingslashit( urlencode( $s ) ) : urlencode( $s );
			$location  = home_url( $location );
			wp_safe_redirect( $location, 301 );
			exit;
		}
	}
	
	// add_action( 'template_redirect', 'superboss_change_search_url', 10, 2 );

	function superboss_search_page_template( $template ) {
		if ( is_search() ) {
			$new_template = locate_template( array( 'searchpage.php' ) );

			if ( '' != $new_template ) {
				return $new_template;
			}
		}

		return $template;
	}

	// add_filter( 'template_include', 'superboss_search_page_template' );
