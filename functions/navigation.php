<?php
	/**
	 * Functions related to navigation
	 *
	 * @package Superboss
	 */

	/* Create various navigation menus
	============================================================================= */

	register_nav_menus([
		'primary_navigation' => __( 'Primary Navigation', 'superboss' ),
		'footer_navigation' => __( 'Footer Navigation', 'superboss' ),
	]);

	// 'utility_navigation' => __( 'Utility Navigation', 'superboss' ),


	/* Add Upload link to main navigation menu
	============================================================================= */

	function superboss_add_shop_to_menu( $items, $args ) {
		if ( 'primary_navigation' != $args->theme_location || ! function_exists( 'woocommerce_get_page_id' ) ) {
			return $items;
		}

		$shop_page_url = get_permalink( woocommerce_get_page_id( 'shop' ) );

		$items .= '<li class="menu-item menu-item-button"><a href="' . $shop_page_url . '">Shop</a></li>';

		return $items;
	}

	// add_filter( 'wp_nav_menu_items', 'superboss_add_shop_to_menu', 199, 2 );


	/* Add icon to top-level nav items with children (Mobile Nav Child Menus)
	============================================================================= */
	
	function superboss_add_icon_to_menu_items_with_children( $output, $item, $depth, $args ) {

		if( 'primary_navigation' == $args->theme_location && $depth === 0 ) {
		
		    if ( in_array( 'menu-item-has-children', $item->classes ) ) {
		        $output .='<button class="menu-item-expand js-menu-item-expand"><i class="material-icons">expand_more</i></button>';
		    }
		}

	    return $output;
	}

	// add_filter( 'walker_nav_menu_start_el', 'superboss_add_icon_to_menu_items_with_children', 10, 4 );