<?php
	/**
	 * Theme Functions
	 *
	 * Add functions specific to this theme here.
	 *
	 * @package Superboss
	 */

	/* Helpers
	============================================================================= */

	function superboss_has_featured_video() {
		$hasVideo = false;
		$video = get_field( 'featured_video' );
		$video = $video[0];

		if ( $video['featured_video_display'] ) {

			switch ( $video['featured_video_type'] ) {
				case 'youtube':
					if ( $video['featured_video_youtube'] ) {
						$hasVideo = true;
					}
					break;

				case 'vimeo':
					if ( $video['featured_video_vimeo'] ) {
						$hasVideo = true;
					}
					break;

				case 'file':
					if ( $video['featured_video_file'] ) {
						$hasVideo = true;
					}
					break;

				default:
			}
		}

		return $hasVideo;
	}
