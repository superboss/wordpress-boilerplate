<?php
	/**
	 * Functions that affect the WordPress admin area.
	 *
	 * @package Superboss
	 */


/* Customize WordPress Admin Login Page
	============================================================================= */
	
	function superboss_custom_login_logo() { ?>
    <style type="text/css">
        #login h1 a, .login h1 a {
            background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/assets/images/site/wp-login-image.svg);
			height: 141px;
			width: 145px;
			background-size: auto;
			background-repeat: no-repeat;
        	padding-bottom: 30px;
        	position: relative;
        	overflow: visible;
        }
        #login h1 a:after, .login h1 a:after {
        	position: absolute;
		    top: 0;
		    right: 0;
		    bottom: 0;
		    left: 0;
		    content: '.';
		    background: #1192D1;
		    filter: blur(120px);
		    display: block;
		    overflow: visible;
		    z-index: -1;
		}
        body.login {
        	background-color: #333;
        }

        body.login form {
        	background-color: #eee;
        	border: none;
        	border-radius: 5px;
        }

        body.login #backtoblog a, 
        body.login #nav a,
        .privacy-policy-link {
        	color: #eee;
        }

        body.login #backtoblog a:hover, 
        body.login #nav a:hover, 
        body.login h1 a:hover,
        .privacy-policy-link:hover {
        	color: #0000cc;
        }
    </style>
<?php }
	
	add_action( 'login_enqueue_scripts', 'superboss_custom_login_logo' );


	function superboss_login_logo_url() {
	    return home_url();
	}
	
	add_filter( 'login_headerurl', 'superboss_login_logo_url' );

	function superboss_login_logo_url_title() {
	    return get_bloginfo('name');
	}

	add_filter( 'login_headertitle', 'superboss_login_logo_url_title' );


	/* Redirect after logout
	============================================================================= */

	function superboss_auto_redirect_after_logout() {
		wp_redirect( home_url() );
		exit();
	}

	add_action( 'wp_logout', 'superboss_auto_redirect_after_logout' );


	/* Create new metabox area
	============================================================================= */

	function superboss_add_metabox_area_under_title() {
		global $post, $wp_meta_boxes;

		do_meta_boxes( get_current_screen(), 'superboss_meta_boxes', $post );

		unset( $wp_meta_boxes['post']['superboss_meta_boxes'] );
	}

	add_action( 'edit_form_after_title', 'superboss_add_metabox_area_under_title' );

	function superboss_add_margin_to_advanced_metaboxes() {
?>
<style>
	#superboss_meta_boxes-sortables { margin-top: 20px;	}
	body #postimagediv .inside img { width: auto; }
</style>
<?php
	}

	add_action( 'admin_head', 'superboss_add_margin_to_advanced_metaboxes' );


	/* Move Featured Image metabox underneath Page Title
	 *
	 * @author Bill Erickson
	 * @link http://www.billerickson.net/code/move-featured-image-metabox
	============================================================================= */

	function superboss_move_featured_image_metabox() {
		remove_meta_box( 'postimagediv', 'post', 'side' );
		remove_meta_box( 'postimagediv', 'page', 'side' );
		add_meta_box( 'postimagediv', __( 'Page Header Image' ), 'post_thumbnail_meta_box', array( 'post', 'page' ), 'superboss_meta_boxes', 'high' );
	}

	add_action( 'do_meta_boxes', 'superboss_move_featured_image_metabox' );


	/* Custom Admin CSS -- Update ACF handles 
	============================================================================= */

	function superboss_custom_admin_css() {
	  echo '<style>
	   
	   .acf-field-accordion .acf-accordion-title,
	   .acf-fc-layout-handle,
	   .post-new-php h2.ui-sortable-handle,
	   .post-php h2.ui-sortable-handle { background-color: #115362; color: white; }

	   .acf-field-accordion .acf-accordion-title p.description { color: #ddd; }

	   .acf-accordion-icon,
	   .acf-flexible-content .layout .acf-fc-layout-handle,
	   .toggle-indicator,
	   .toggle-indicator:before,
	   .acf-flexible-content .layout .acf-fc-layout-controls .acf-icon.-collapse:hover { color: white; }

	   .acf-flexible-content .layout .acf-fc-layout-controls .acf-icon.-collapse:hover { border-color: white; }

	   .acf-table { border-collapse: collapse; }
	   .acf-table .acf-row { border: 2px solid #BBBBBB; }

	  </style>';
	}

	// add_action('admin_head', 'superboss_custom_admin_css');
