<?php
	/**
	 * Main Setup
	 *
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 *
	 * @package Superboss
	 */

	function superboss_setup() {

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		// Let WordPress manage the document title.
		// By adding theme support, we declare that this theme does not use a
		// hard-coded <title> tag in the document head, and expect WordPress to provide it for us.
		add_theme_support( 'title-tag' );

		// Switch default core markup for search form, comment form, and comments to output valid HTML5.
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		// Requires Soil plugin
		// add_theme_support( 'soil-clean-up' );
		// add_theme_support( 'soil-nice-search' );
		// add_theme_support( 'soil-relative-urls' );
		// add_theme_support( 'soil-jquery-cdn' );

		add_post_type_support( 'page', 'excerpt' );
	}

	add_action( 'after_setup_theme', 'superboss_setup' );
