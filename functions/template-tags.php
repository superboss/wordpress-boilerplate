<?php
	/**
	 * Custom template tags for this theme
	 *
	 * Eventually, some of the functionality here could be replaced by core features.
	 *
	 * @package Superboss
	 */

	/* Print array as JSON
	============================================================================= */

	function superboss_json_attribute( $data ) {
		if ( is_array( $data ) ) {
			$data = json_encode( $data );
		}
		return htmlentities( $data );
	}


	/* Print array as JSON
	============================================================================= */

	function superboss_build_tax_query( $query_vars = array() ) {
		$query = array();

		foreach ( $query_vars as $key => $val ) {
			$query[] = array(
				'taxonomy' => $key,
				'field' => 'slug',
				'terms' => $val,
			);
		}

		return $query;
	}


	/* Video embed URL helpers
	============================================================================= */

	function superboss_get_vimeo_embed_id( $url ) {
		$pattern = '/(?:www\.|player\.)?vimeo.com\/(?:channels\/(?:\w+\/)?|groups\/([^\/]*)\/videos\/|album\/(\d+)\/video\/|video\/|)(\d+)(?:$|\/|\?)/';
		$embed_id = false;

		if ( preg_match( $pattern, $url, $match ) ) {
			$embed_id = $match[3];
		}

		return $embed_id;
	}

	function superboss_get_vimeo_embed_link( $url ) {
		$pattern = '/(?:www\.|player\.)?vimeo.com\/(?:channels\/(?:\w+\/)?|groups\/([^\/]*)\/videos\/|album\/(\d+)\/video\/|video\/|)(\d+)(?:$|\/|\?)/';
		$embed_url = false;

		if ( preg_match( $pattern, $url, $match ) ) {
			$embed_url = 'player.vimeo.com/video/' . $match[3];
		}

		return $embed_url;
	}

	function superboss_get_youtube_embed_link( $url ) {
		$pattern = '/^.*(?:(?:youtu\.be\/|v\/|vi\/|u\/\w\/|embed\/)|(?:(?:watch)?\?v(?:i)?=|\&v(?:i)?=))([^#\&\?]*).*/';
		$embed_url = false;

		if ( preg_match( $pattern, $url, $match ) ) {
			$embed_url = 'www.youtube.com/embed/' . $match[1];
		}

		return $embed_url;
	}

	function superboss_get_youtube_thumbnail_link( $url ) {
		$pattern = '/^.*(?:(?:youtu\.be\/|v\/|vi\/|u\/\w\/|embed\/)|(?:(?:watch)?\?v(?:i)?=|\&v(?:i)?=))([^#\&\?]*).*/';
		$img_url = false;

		if ( preg_match( $pattern, $url, $match ) ) {
			$img_url = 'https://img.youtube.com/vi/' . $match[1] . '/mqdefault.jpg';
		}

		return $img_url;
	}


	/* Prints HTML with meta information for the current post-date/time.
	============================================================================= */

	function superboss_posted_on() {
		$time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';
		if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
			$time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time><time class="updated" datetime="%3$s">%4$s</time>';
		}

		$time_string = sprintf( $time_string,
			esc_attr( get_the_date( 'c' ) ),
			esc_html( get_the_date() ),
			esc_attr( get_the_modified_date( 'c' ) ),
			esc_html( get_the_modified_date() )
		);

		$posted_on = sprintf(
			/* translators: %s: post date. */
			esc_html_x( 'Posted on %s', 'post date', 'superboss' ),
			'<a href="' . esc_url( get_permalink() ) . '" rel="bookmark">' . $time_string . '</a>'
		);

		echo '<span class="posted-on">' . $posted_on . '</span>'; // WPCS: XSS OK.
	}


	/* Prints HTML with meta information for the current author.
	============================================================================= */

	function superboss_posted_by() {
		$byline = sprintf(
			/* translators: %s: post author. */
			esc_html_x( 'by %s', 'post author', 'superboss' ),
			'<span class="author vcard"><a class="url fn n" href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '">' . esc_html( get_the_author() ) . '</a></span>'
		);

		echo '<span class="byline"> ' . $byline . '</span>'; // WPCS: XSS OK.
	}


	/* Prints HTML with meta information for the categories, tags and comments.
	============================================================================= */

	function superboss_entry_footer() {
		// Hide category and tag text for pages.
		if ( 'post' === get_post_type() ) {
			/* translators: used between list items, there is a space after the comma */
			$categories_list = get_the_category_list( esc_html__( ', ', 'superboss' ) );
			if ( $categories_list ) {
				/* translators: 1: list of categories. */
				printf( '<span class="cat-links">' . esc_html__( 'Posted in %1$s', 'superboss' ) . '</span>', $categories_list ); // WPCS: XSS OK.
			}

			/* translators: used between list items, there is a space after the comma */
			$tags_list = get_the_tag_list( '', esc_html_x( ', ', 'list item separator', 'superboss' ) );
			if ( $tags_list ) {
				/* translators: 1: list of tags. */
				printf( '<span class="tags-links">' . esc_html__( 'Tagged %1$s', 'superboss' ) . '</span>', $tags_list ); // WPCS: XSS OK.
			}
		}

		if ( ! is_single() && ! post_password_required() && ( comments_open() || get_comments_number() ) ) {
			echo '<span class="comments-link">';
			comments_popup_link(
				sprintf(
					wp_kses(
						/* translators: %s: post title */
						__( 'Leave a Comment<span class="screen-reader-text"> on %s</span>', 'superboss' ),
						array(
							'span' => array(
								'class' => array(),
							),
						)
					),
					get_the_title()
				)
			);
			echo '</span>';
		}

		edit_post_link(
			sprintf(
				wp_kses(
					/* translators: %s: Name of current post. Only visible to screen readers */
					__( 'Edit <span class="screen-reader-text">%s</span>', 'superboss' ),
					array(
						'span' => array(
							'class' => array(),
						),
					)
				),
				get_the_title()
			),
			'<span class="edit-link">',
			'</span>'
		);
	}


	/* Displays an optional post thumbnail.
	 * Wraps the post thumbnail in an anchor element on
	 * index views, or a div element when on single views.
	============================================================================= */

	function superboss_post_thumbnail() {
		if ( post_password_required() || is_attachment() || ! has_post_thumbnail() ) {
			return;
		}

		if ( is_singular() ) :
		?>
		<div class="post-thumbnail">
			<?php the_post_thumbnail(); ?>
		</div><!-- .post-thumbnail -->
		<?php else : ?>
		<a class="post-thumbnail" href="<?php the_permalink(); ?>" aria-hidden="true">
			<?php
				the_post_thumbnail( 'post-thumbnail', array(
					'alt' => the_title_attribute( array(
						'echo' => false,
					) ),
				) );
			?>
		</a>
		<?php
		endif; // End is_singular().
	}

	/* Draw Pagination Links for Custom Post Type
============================================================================= */

function superboss_posts_pagination( $custom_query ) {
	global $wp_query;

	if ( !isset($custom_query) ) :
		$custom_query = $wp_query;
	endif;

	if ( !$current_page = get_query_var( 'paged' ) ) {
		$current_page = 1;
	}

	$big = 999999999; // need an unlikely integer

	$permalinks = get_option( 'permalink_structure' );

	if( is_front_page() ) {
		$format = empty( $permalinks ) ? '?paged=%#%' : 'page/%#%/';
	} else {
		$format = empty( $permalinks ) || is_search() ? '&paged=%#%' : 'page/%#%/';
	}

	$base = str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) );

	$pagination = paginate_links( array(
		'base' => $base,
		'format' => $format,
		'current' => $current_page,
		'total' => $custom_query->max_num_pages,
		'end_size' => '1',
		'mid_size' => '4',
		'type' => 'array',
		'prev_next' => false,
	) );

	if ( ! $pagination ) :
		return false;
	else :	
		if ( 1 === $current_page ) :
			$prev = '<span class="page-numbers pagenav prev">Previous</span>';
		else :
			$prev = str_replace( '%_%', $format, $base );
			$prev = str_replace( '%#%', $current_page - 1, $prev );
			$prev = '<a href="' . esc_url( apply_filters( 'paginate_links', $prev ) ) . '" class="page-numbers pagenav prev">Previous</a>';
		endif;

		if ( $custom_query->max_num_pages > $current_page ) :
			$next = str_replace( '%_%', $format, $base );
			$next = str_replace( '%#%', $current_page + 1, $next );
			$next = '<a href="' . esc_url( apply_filters( 'paginate_links', $next ) ) . '" class="page-numbers pagenav next">Next</a>';
		else :
			$next = '<span class="page-numbers pagenav next">Next</span>';
		endif;

		$html = "";
		$html .= "<ul class='pagination'>\n\t<li>";
		$html .= $prev . "</li>\n\t<span class='pagination-wrap-pages'>\n\t\t<li>";
		$html .= join( "</li>\n\t\t<li>", $pagination );
		$html .= "</li>\n\t</span>\n\t<li>" . $next;
		$html .= "</li>\n</ul>\n";

		return $html;
	endif;
}


