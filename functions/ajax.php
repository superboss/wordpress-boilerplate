<?php
	/**
	 * Actions for AJAX functions on front-end.
	 *
	 * @package Superboss
	 */

	function yoyo_enqueue_ajax_scripts() {
		wp_enqueue_script( 'superboss-ajax', get_template_directory_uri() . '/assets/js/sb-ajax.js', array( 'jquery' ), null, true );
		wp_localize_script( 'superboss-ajax', 'wpAJAX', array(
			'ajaxurl' => admin_url( 'admin-ajax.php' ),
			'security' => wp_create_nonce( 'ajax-action' ),
		));
	}

	// add_action( 'wp_enqueue_scripts', 'yoyo_enqueue_ajax_scripts' );
	

	// function yoyo_build_query( $filters = array() ) {

	// 	$tax = false;

	// 	$query = array();

	// 	$tax_query = array(
	// 		'relation' => 'AND',
	// 	);

	// 	$search_query = array();

	// 	foreach ( $filters as $filter ) {

	// 		if ( $filter['name'] == 's' ) {
	// 			$query['s'] = $filter['value'];
	// 		} else {

	// 			if ( ! $tax ) : $tax = 1; endif;

	// 			$taxonomy = $filter['name'];
	// 			$terms    = $filter['value'];

	// 			if ( 'all' !== $terms ) {
	// 				$tax_query[] = array(
	// 					'taxonomy' => $taxonomy,
	// 					'field' => 'slug',
	// 					'terms' => $terms,
	// 				);
	// 			}
	// 		}

	// 		if ( $tax ) {
	// 			$query['tax_query'] = $tax_query;
	// 		}
	// 	}

	// 	return $query;
	// }

	// function yoyo_get_posts_count() {
	// 	global $wp_query, $posts;

	// 	check_ajax_referer( 'ajax-action', 'security' );

	// 	$count = 0;

	// 	$post_type = isset( $_POST['type'] )    ? $_POST['type']    : 'post';
	// 	$filters   = isset( $_POST['filters'] ) ? $_POST['filters'] : false;

	// 	$archive_args = array(
	// 		'posts_per_page' => -1,
	// 		'post_type' => $post_type,
	// 	);

	// 	if ( $filters ) {
	// 		$archive_args = array_merge( $archive_args, yoyo_build_query( $filters ) );
	// 	}

	// 	$posts = get_posts( $archive_args );

	// 	echo count( $posts );

	// 	die();
	// }

	// function yoyo_get_blog_posts() {
	// 	global $wp_query, $posts;

	// 	check_ajax_referer( 'ajax-action', 'security' );

	// 	$paged   = isset( $_POST['paged'] ) ? $_POST['paged'] : false;
	// 	$filters = isset( $_POST['filters'] ) ? $_POST['filters'] : false;

	// 	$archive_args = array(
	// 		'posts_per_page' => 9,
	// 		'post_type' => 'post',
	// 		'orderby' => 'date',
	// 		'paged' => $paged,
	// 	);

	// 	if ( $filters ) {
	// 		$archive_args = array_merge( $archive_args, yoyo_build_query( $filters ) );
	// 	}

	// 	$posts = get_posts( $archive_args );

	// 	if ( is_array( $posts ) && ! empty( $posts ) ) {
	// 		get_template_part( 'template-parts/content-posts', 'list' );
	// 	}

	// 	die();
	// }

	// function yoyo_get_tricks_posts() {
	// 	global $wp_query;

	// 	check_ajax_referer( 'ajax-action', 'security' );

	// 	$paged   = isset( $_POST['paged'] )   ? $_POST['paged']   : false;
	// 	$filters = isset( $_POST['filters'] ) ? $_POST['filters'] : false;

	// 	$archive_args = array(
	// 		'posts_per_page' => 1,
	// 		'post_type' => 'tricks',
	// 		'orderby' => 'menu_order',
	// 		'order' => 'ASC',
	// 		'paged' => $paged,
	// 	);

	// 	if ( $filters ) {
	// 		$archive_args = array_merge( $archive_args, yoyo_build_tax_query( $filters ) );
	// 	}

	// 	$tricks = new WP_Query( $archive_args );
		
	// 	while ( $tricks->have_posts() ) {
	// 		$tricks->the_post();
	// 		get_template_part( 'template-parts/content-post', 'tricks' );
	// 	}

	// 	die();
	// }

	// function yoyo_get_search_results() {
	// 	global $wp_query, $product_results, $trick_results, $post_results, $brand_results, $page_results;

	// 	check_ajax_referer( 'ajax-action', 'security' );

	// 	$query = isset( $_POST['s'] ) ? $_POST['s'] : false;

	// 	$product_search_args = array(
	// 		'posts_per_page' => -1,
	// 		'post_type' => 'product',
	// 		'post_status' => 'publish',
	// 		'orderby' => 'menu_order',
	// 		'order' => 'ASC',
	// 		's' => $query
	// 	);

	// 	$product_results = new WP_Query( $product_search_args );


	// 	$trick_search_args = array(
	// 		'posts_per_page' => -1,
	// 		'post_type' => 'tricks',
	// 		'post_status' => 'publish',
	// 		'orderby' => 'menu_order',
	// 		'order' => 'ASC',
	// 		's' => $query
	// 	);

	// 	$trick_results = new WP_Query( $trick_search_args );


	// 	$post_search_args = array(
	// 		'posts_per_page' => -1,
	// 		'post_type' => 'post',
	// 		'post_status' => 'publish',
	// 		'orderby' => 'date',
	// 		's' => $query
	// 	);

	// 	$post_results = new WP_Query( $post_search_args );

	// 	$html = '';

	// 	$count = 0;

	// 	if ( $product_results && $product_results->have_posts() ) :
	// 		$html .= '<span class="search-results-hdr">Products</span>';
	// 		$html .= '<ul class="results-list">';

	// 		while ( $product_results->have_posts() ) : $product_results->the_post();
	// 			$image = wp_get_attachment_image_src( get_post_thumbnail_id(), 'square-small' );
	// 			$image = $image[0];

	// 			$html .= '<li>';
	// 			$html .= '<a href="' . get_the_permalink() .'" class="results">';
	// 			$html .= '<div class="results-img">';
	// 			$html .= '<img src="'. $image .'">';
	// 			$html .= '</div>';
	// 			$html .= '<div class="results-desc">';
	// 			$html .= '<span class="results-title">' . get_the_title() . '</span>';
	// 			$html .= '</div>';
	// 			$html .= '</a>';
	// 			$html .= '</li>';

	// 			$count++;
	// 		endwhile;

	// 		$html .= '</ul>';

	// 	endif;

	// 	if ( $trick_results && $trick_results->have_posts() ) :
	// 		$html .= '<span class="search-results-hdr">Tricks</span>';
	// 		$html .= '<ul class="results-list">';

	// 		while ( $trick_results->have_posts() ) : $trick_results->the_post();
	// 			$image = wp_get_attachment_image_src( get_post_thumbnail_id(), 'square-small' );
	// 			$image = $image[0];

	// 			$html .= '<li>';
	// 			$html .= '<a href="' . get_the_permalink() .'" class="results">';
	// 			$html .= '<div class="results-img">';
	// 			$html .= '<img src="'. $image .'">';
	// 			$html .= '</div>';
	// 			$html .= '<div class="results-desc">';
	// 			$html .= '<span class="results-title">' . get_the_title() . '</span>';
	// 			$html .= '</div>';
	// 			$html .= '</a>';
	// 			$html .= '</li>';

	// 			$count++;
	// 		endwhile;

	// 		$html .= '</ul>';
		
	// 	endif;

	// 	if ( $post_results && $post_results->have_posts() ) :
	// 		$html .= '<span class="search-results-hdr">Blog</span>';
	// 		$html .= '<ul class="results-list">';
	
	// 		while ( $post_results->have_posts() ) : $post_results->the_post();
	// 			$image = wp_get_attachment_image_src( get_post_thumbnail_id(), 'square-small' );
	// 			$image = $image[0];
				
	// 			$html .= '<li>';
	// 			$html .= '<a href="' . get_the_permalink() .'" class="results">';
	// 			$html .= '<div class="results-img">';
	// 			$html .= '<img src="'. $image .'">';
	// 			$html .= '</div>';
	// 			$html .= '<div class="results-desc">';
	// 			$html .= '<span class="results-title">' . get_the_title() . '</span>';
	// 			$html .= '</div>';
	// 			$html .= '</a>';
	// 			$html .= '</li>';

	// 			$count++;
	// 		endwhile;

	// 		$html .= '</ul>';

	// 	endif;

	// 	$s = 's';
	// 	if ( $count === 1 ) {
	// 		$s = '';
	// 	}

	// 	echo '<span class="tally js-result-count">' . $count . ' Result' . $s . '</span>' . $html;

	// 	die();
	// }

	// if ( is_admin() ) {
	// 	add_action( 'wp_ajax_get_posts_count',        'yoyo_get_posts_count' );
	// 	add_action( 'wp_ajax_nopriv_get_posts_count', 'yoyo_get_posts_count' );

	// 	add_action( 'wp_ajax_get_blog_posts',        'yoyo_get_blog_posts' );
	// 	add_action( 'wp_ajax_nopriv_get_blog_posts', 'yoyo_get_blog_posts' );

	// 	add_action( 'wp_ajax_get_tricks_posts',        'yoyo_get_tricks_posts' );
	// 	add_action( 'wp_ajax_nopriv_get_tricks_posts', 'yoyo_get_tricks_posts' );

	// 	add_action( 'wp_ajax_get_search_results',        'yoyo_get_search_results' );
	// 	add_action( 'wp_ajax_nopriv_get_search_results', 'yoyo_get_search_results' );
	// }
