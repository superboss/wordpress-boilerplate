<?php
	/**
	 * Head
	 *
	 * Functions related to the <head> tag, run in wp_head()
	 *
	 * @package Superboss
	 */


	/* Add pingback URL auto-discovery header for singular articles.
	============================================================================= */

	function superboss_pingback_header() {
		if ( is_singular() && pings_open() ) {
			echo '<link rel="pingback" href="', esc_url( get_bloginfo( 'pingback_url' ) ), '">';
		}
	}

	add_action( 'wp_head', 'superboss_pingback_header' );
