<?php
	/**
	 * Plugins
	 *
	 * Add functions related to plugins here.
	 *
	 * @package Superboss
	 */

	/* Move Yoast SEO Metabox to Bottom of Admin Page
	============================================================================= */

	function superboss_yoast_to_bottom() {
		return 'low';
	}

	// add_filter( 'wpseo_metabox_prio', 'superboss_yoast_to_bottom' );


	/* Add Google Analytics script
	============================================================================= */

	// add_theme_support( 'soil-google-analytics', 'UA-' . GOOGLE_ANALYTICS_ACCOUNT, 'wp_footer' );


	/* Tell Gravity Forms to use <button> instead of <input> for submit
	============================================================================= */

	function superboss_gravity_forms_submit_button( $button, $form ) {
		return "<button class='button gform_button' id='gform_submit_button_{$form['id']}'><span>Submit</span></button>";
	}

	add_filter( 'gform_submit_button', 'superboss_gravity_forms_submit_button', 10, 2 );


	/* Adjust Gravity Forms markup surrounding file upload input field
	============================================================================= */

	function superboss_gravity_forms_change_upload_input_markup( $file_upload_markup, $file_info, $form_id, $field_id ) {
		return '<div class="gform_file_upload_wrapper">' + $file_upload_markup + '</div>';
	}

	add_filter( 'gform_file_upload_markup', 'superboss_gravity_forms_change_upload_input_markup', 10, 4 );


	/* Reassign tabindex for Gravity Forms
	============================================================================= */

	function superboss_gravity_forms_change_tabindex( $tabindex, $form ) {
		return 0;
	}

	add_filter( 'gform_tabindex', 'superboss_gravity_forms_change_tabindex' , 10, 2 );


	/* Speed up post edit pages using ACF
	============================================================================= */
	
	add_filter('acf/settings/remove_wp_meta_box', '__return_true');
