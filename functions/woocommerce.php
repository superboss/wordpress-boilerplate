<?php
	/**
	 * WooCommerce Compatibility File
	 *
	 * @link https://woocommerce.com/
	 *
	 * @package Superboss
	 */

	/*
	 * WooCommerce setup function.
	 * @link https://docs.woocommerce.com/document/third-party-custom-theme-compatibility/
	 * @link https://github.com/woocommerce/woocommerce/wiki/Enabling-product-gallery-features-(zoom,-swipe,-lightbox)-in-3.0.0
	 *
	 * @return void
	============================================================================= */

	function superboss_woocommerce_setup() {
		add_theme_support( 'woocommerce' );
		add_theme_support( 'wc-product-gallery-zoom' );
		add_theme_support( 'wc-product-gallery-lightbox' );
		add_theme_support( 'wc-product-gallery-slider' );
	}

	add_action( 'after_setup_theme', 'superboss_woocommerce_setup' );


	/*
	 * WooCommerce specific scripts & stylesheets.
	 *
	 * @return void
	============================================================================= */

	/*

	function superboss_woocommerce_scripts() {
		wp_enqueue_style( 'superboss-woocommerce-style', get_template_directory_uri() . '/woocommerce.css' );

		$font_path   = WC()->plugin_url() . '/assets/fonts/';
		$inline_font = '@font-face {
				font-family: "star";
				src: url("' . $font_path . 'star.eot");
				src: url("' . $font_path . 'star.eot?#iefix") format("embedded-opentype"),
					url("' . $font_path . 'star.woff") format("woff"),
					url("' . $font_path . 'star.ttf") format("truetype"),
					url("' . $font_path . 'star.svg#star") format("svg");
				font-weight: normal;
				font-style: normal;
			}';

		wp_add_inline_style( 'superboss-woocommerce-style', $inline_font );
	}

	add_action( 'wp_enqueue_scripts', 'superboss_woocommerce_scripts' );
	
	*/

	/*
	 * Disable the default WooCommerce stylesheet.
	 *
	 * Removing the default WooCommerce stylesheet and enqueing your own will
	 * protect you during WooCommerce core updates.
	 *
	 * @link https://docs.woocommerce.com/document/disable-the-default-stylesheet/
	============================================================================= */

	// add_filter( 'woocommerce_enqueue_styles', '__return_empty_array' );


	/*
	 * Add 'woocommerce-active' class to the body tag.
	 *
	 * @param  array $classes CSS classes applied to the body tag.
	 * @return array $classes modified to include 'woocommerce-active' class.
	============================================================================= */

	function superboss_woocommerce_active_body_class( $classes ) {
		$classes[] = 'woocommerce-active';

		return $classes;
	}

	add_filter( 'body_class', 'superboss_woocommerce_active_body_class' );


	/*
	 * Products per page.
	 *
	 * @return integer number of products.
	============================================================================= */

	function superboss_woocommerce_products_per_page() {
		return 12;
	}

	add_filter( 'loop_shop_per_page', 'superboss_woocommerce_products_per_page' );


	/*
	 * Product gallery thumnbail columns.
	 *
	 * @return integer number of columns.
	============================================================================= */

	function superboss_woocommerce_thumbnail_columns() {
		return 4;
	}

	add_filter( 'woocommerce_product_thumbnails_columns', 'superboss_woocommerce_thumbnail_columns' );


	/*
	 * Default loop columns on product archives.
	 *
	 * @return integer products per row.
	============================================================================= */

	function superboss_woocommerce_loop_columns() {
		return 3;
	}

	add_filter( 'loop_shop_columns', 'superboss_woocommerce_loop_columns' );


	/*
	 * Return formatted address string for store location
	 *
	 * @return array $address formatted address string
	============================================================================= */

	function superboss_woocommerce_get_store_address( $insert_breaks = false ) {
		$address_1     = get_option( 'woocommerce_store_address' );
		$address_2     = get_option( 'woocommerce_store_address_2' );
		$city          = get_option( 'woocommerce_store_city' );
		$country_state = wc_get_base_location();
		$zip           = get_option( 'woocommerce_store_postcode' );

		$address = $address_1;

		if( $address_2 ) :
			if ( $insert_breaks ) :
				$address .= '<br>';
			endif;

			$address .= ' ' . $address_2;
		endif;

		if ( $insert_breaks ) :
			$address .= '<br>';
		else :
			$address .= ' – ';
		endif;

		$address .= $city;

		if( is_array( $country_state ) && ! empty( $country_state ) ) :
			$address .= ', ' . $country_state['state'];
		endif;

		$address .= ' ' . $zip;

		if ($address) :
			return $address;
		else :
			return false;
		endif;
	}


	/*
	 * Format breadcrumbs markup
	 *
	 * @return array of breadcrumbs
	 ============================================================================= */

	function superboss_woocommerce_breadcrumbs() {
		return array(
			'delimiter'   => '',
			'wrap_before' => '<ul class="breadcrumbs-list">',
			'wrap_after'  => '</ul>',
			'before'      => '<li>',
			'after'       => '</li>',
			'home'        => _x( 'Home', 'breadcrumb', 'woocommerce' ),
		);
	}

	add_filter( 'woocommerce_breadcrumb_defaults', 'superboss_woocommerce_breadcrumbs' );


	/*
	 * Related Products Args.
	 *
	 * @param array $args related products args.
	 * @return array $args related products args.
	============================================================================= */

	function superboss_woocommerce_related_products_args( $args ) {
		$defaults = array(
			'posts_per_page' => 3,
			'columns'        => 3,
		);

		$args = wp_parse_args( $defaults, $args );

		return $args;
	}

	add_filter( 'woocommerce_output_related_products_args', 'superboss_woocommerce_related_products_args' );


	/*
	 * Adjust Product columns wrapper
	 ============================================================================= */

	if ( ! function_exists( 'superboss_woocommerce_product_columns_wrapper' ) ) {
		/**
		 * Product columns wrapper.
		 *
		 * @return  void
		 */
		function superboss_woocommerce_product_columns_wrapper() {
			$columns = superboss_woocommerce_loop_columns();
			echo '<div class="columns-' . absint( $columns ) . '">';
		}
	}

	add_action( 'woocommerce_before_shop_loop', 'superboss_woocommerce_product_columns_wrapper', 40 );


	if ( ! function_exists( 'superboss_woocommerce_product_columns_wrapper_close' ) ) {
		/**
		 * Product columns wrapper close.
		 *
		 * @return  void
		 */
		function superboss_woocommerce_product_columns_wrapper_close() {
			echo '</div>';
		}
	}

	add_action( 'woocommerce_after_shop_loop', 'superboss_woocommerce_product_columns_wrapper_close', 40 );


	/*
	 * Remove default WooCommerce wrapper.
	 ============================================================================= */

	remove_action( 'woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10 );
	remove_action( 'woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10 );

	if ( ! function_exists( 'superboss_woocommerce_wrapper_before' ) ) {
		/**
		 * Before Content.
		 *
		 * Wraps all WooCommerce content in wrappers which match the theme markup.
		 *
		 * @return void
		 */
		function superboss_woocommerce_wrapper_before() {
?>
			<div id="primary" class="content-area">
				<main id="main" class="site-main" role="main">
<?php
		}
	}

	add_action( 'woocommerce_before_main_content', 'superboss_woocommerce_wrapper_before' );


	if ( ! function_exists( 'superboss_woocommerce_wrapper_after' ) ) {
		/**
		 * After Content.
		 *
		 * Closes the wrapping divs.
		 *
		 * @return void
		 */
		function superboss_woocommerce_wrapper_after() {
?>
				</main><!-- #main -->
			</div><!-- #primary -->
<?php
		}
	}

	add_action( 'woocommerce_after_main_content', 'superboss_woocommerce_wrapper_after' );


	/*
	 * Custom Superboss theme functions
	============================================================================= */

	function superboss_woocommerce_get_product_thumbnail( $id = -1, $size = 'product-callout-thumbnail' ) {
		global $post, $product;

		if ( $id == -1 ) {
			if ( $product ) {
				$id = $product->ID;
			} else if ( $post ) {
				$id = $post->ID;
			}
		}

		$image_size = apply_filters( 'single_product_archive_thumbnail_size', $size );

		echo get_the_post_thumbnail_url( $id, $image_size );
	}

	function superboss_woocommerce_get_variation_url() {
		global $post;

		$url;
		return $url;
	}

