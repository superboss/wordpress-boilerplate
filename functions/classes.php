<?php
	/**
	 * DOM Class Functions
	 *
	 * Add functions related to front-end system classes
	 *
	 * @package Superboss
	 */

	/* Add Formstone grid class 'fs-grid' to, remove 'page' from <body>
	============================================================================= */

	function superboss_add_grid_class_to_body( $classes ) {
		$classes[] = 'fs-grid';
		$classes = array_diff( $classes, array( 'page' ) );

		return $classes;
	}

	// add_filter( 'body_class', 'superboss_add_grid_class_to_body' );


	/* Remove 'page' class from posts
	============================================================================= */

	function superboss_remove_page_class_from_post_class( $classes ) {
		$classes = array_diff( $classes, array( 'page' ) );
		$classes[] = 'page-content';

		return $classes;
	}

	add_filter( 'post_class', 'superboss_remove_page_class_from_post_class' );
