<?php
	/**
	 * Image Functions
	 *
	 * @package Superboss
	 */


	/* Enable support for Post Thumbnails on posts and pages.
	============================================================================= */

	add_theme_support( 'post-thumbnails' );


	/* Add image crop sizes
	============================================================================= */

	function superboss_add_image_sizes() {

		// Super-wide
		add_image_size( 'superwide-xlarge', 1440, 1440 * 0.25, true );

		// 16:9
		add_image_size( 'wide-xsmall', 295,  165, true );
		add_image_size( 'wide-small',  360,  202, true );
		add_image_size( 'wide-medium', 470,  264, true );
		add_image_size( 'wide-large',  770,  433, true );
		add_image_size( 'wide-xlarge', 1440, 810, true );
		
		// 3:2 Ratio
		add_image_size( 'ratio-3-2-small',  300,  200,  true );
		add_image_size( 'ratio-3-2-medium', 480,  320,  true );
		add_image_size( 'ratio-3-2-large',  768,  512,  true );
		add_image_size( 'ratio-3-2-xlarge', 1440, 960,  true );

		// 4:3 Ratio
		add_image_size( 'ratio-4-3-small',  300,  225,  true );
		add_image_size( 'ratio-4-3-medium', 480,  360,  true );
		add_image_size( 'ratio-4-3-large',  740,  555,  true );
		add_image_size( 'ratio-4-3-xlarge', 1440, 1080, true );

		// Tall
		add_image_size( 'tall-small',  200, 200 * 1.5, true );
		add_image_size( 'tall-large',  400, 400 * 1.5, true );
		add_image_size( 'tall-xlarge', 960, 960 * 1.5, true );

		// Square
		add_image_size( 'square-xsmall', 160, 160, true );
		add_image_size( 'square-small',  300, 300, true );
		add_image_size( 'square-medium', 480, 480, true );
		add_image_size( 'square-large',  800, 800, true );

		/*

		Additional Sizes

		// 2:1
		add_image_size( 'wide-small',  320,  160, true );
		add_image_size( 'wide-medium', 480,  240, true );
		add_image_size( 'wide-large',  768,  384, true );
		add_image_size( 'wide-xlarge', 1440, 720, true );

		// 74% Ratio
		add_image_size( 'ratio-74-xlarge', 1024, 1024 * 0.74, true );

		// Content Block Images
		add_image_size( 'content-block-small',  300,  315, true );
		add_image_size( 'content-block-large',  525,  550, true );
		add_image_size( 'content-block-retina', 1050, 1100, true );

		// Golden Ratio
		add_image_size( 'ratio-gold-small',  300,  300 *  0.618, true );
		add_image_size( 'ratio-gold-medium', 470,  470 *  0.618, true );
		add_image_size( 'ratio-gold-large',  740,  740 *  0.618, true );
		add_image_size( 'ratio-gold-xlarge', 1440, 1440 * 0.618, true );

		*/
	}

	add_action( 'init', 'superboss_add_image_sizes', 0 );


	/* Build responsive image background for Formstone Background
	============================================================================= */

	function superboss_responsive_background( $images = false ) {
		if ( ! $images ) {
			return false;
		}

		return htmlentities( json_encode( array( 'source' => $images ) ) );
	}


	/* Build responsive image for Picturefill 2.3.1
	============================================================================= */

	function superboss_responsive_image( $images = false, $class = '', $alt = '' ) {
		if ( ! $images ) {
			return false;
		}

		$images = array_reverse( $images );
		$html_all = array();
		$html_ie8 = array();

		foreach ( $images as $media => $image ) {
			if ( 'fallback' !== $media ) {
				$html_all[] = '<source media="' . $media . '" srcset="' . $image . '">';
				$html_ie8[] = '<span class="mimeo-source" media="' . $media . '" srcset="' . $image . '"></span>';
			} else {
				$fallback = $image;
				$html_all[] = '<source media="(min-width: 0px)" srcset="' . $image . '">';
				$html_ie8[] = '<span class="mimeo-source" media="(min-width: 0px)" srcset="' . $image . '"></span>';
			}
		}

		$html = '';
		$html .= '<picture class="responsive-image ' . $class . '">';
		$html .= '<!--[if IE 9]><video style="display: none;"><![endif]-->';
		$html .= implode( '', $html_all );

		$html .= '<!--[if IE 9]></video><![endif]-->';
		$html .= '<img src="' . $fallback . '" alt="' . $alt . '" draggable="false">';
		$html .= '</picture>';

		return $html;
	}

	function superboss_get_image ( $image ) {
		if ( ! $image ) {
			$image = get_field( 'placeholder_image', 'option' );
		}

		return $image;
	}


	/* Image crop sets
	============================================================================= */

		function superboss_images_background_example( $image ) {
		if ( ! $image ) {
			return false;
		}

		$wide_medium = wp_get_attachment_image_src( $image, 'wide-medium' );
		$wide_large  = wp_get_attachment_image_src( $image, 'wide-large' );
		$wide_xlarge = wp_get_attachment_image_src( $image, 'wide-xlarge' );

		return array(
			'0px'    => $wide_medium[0],
			'500px'  => $wide_large[0],
			'980px'  => $wide_xlarge[0],
		);
	}


	function superboss_images_embed_example( $image ) {
		if ( ! $image ) {
			return false;
		}

		$square_medium = wp_get_attachment_image_src( $image, 'square-medium' );
		$square_large  = wp_get_attachment_image_src( $image, 'square-large' );
		$square_retina = wp_get_attachment_image_src( $image, 'square-retina' );

		return array(
			'fallback'           => $square_medium[0],
			'(min-width: 980px)' => $square_large[0] . ', ' . $square_retina[0] . ' 2x',
		);
	}


	function superboss_images_embed_square( $image ) {
		if ( ! $image ) {
			return false;
		}

		$square_medium = wp_get_attachment_image_src( $image, 'square-medium' );
		$square_large  = wp_get_attachment_image_src( $image, 'square-large' );

		return array(
			'fallback'           => $square_medium[0]
		);
	}

	function superboss_images_embed_wide( $image ) {
		if ( ! $image ) {
			return false;
		}

		$wide_medium = wp_get_attachment_image_src( $image, 'wide-medium' );
		$wide_large  = wp_get_attachment_image_src( $image, 'wide-large' );
		$wide_xlarge = wp_get_attachment_image_src( $image, 'wide-xlarge' );

		return array(
			'fallback'           => $wide_medium[0],
			'(min-width: 500px)' => $wide_large[0],
			'(min-width: 980px)' => $wide_xlarge[0],
		);
	}

	function superboss_images_embed_hero( $image ) {
		if ( ! $image ) {
			return false;
		}

		$tall_large = wp_get_attachment_image_src( $image, 'tall-large' );
		$wide_large  = wp_get_attachment_image_src( $image, 'wide-large' );
		$wide_xlarge = wp_get_attachment_image_src( $image, 'wide-xlarge' );

		return array(
			'fallback'           => $tall_large[0],
			'(min-width: 500px)' => $wide_large[0],
			'(min-width: 980px)' => $wide_xlarge[0],
		);
	}



	function superboss_images_embed_superwide( $image ) {
		if ( ! $image ) {
			return false;
		}

		$wide_medium      = wp_get_attachment_image_src( $image, 'wide-medium' );
		$wide_large       = wp_get_attachment_image_src( $image, 'wide-large' );
		$superwide_xlarge = wp_get_attachment_image_src( $image, 'superwide-xlarge' );

		return array(
			'fallback'           => $wide_medium[0],
			'(min-width: 500px)' => $wide_large[0],
			'(min-width: 980px)' => $superwide_xlarge[0],
		);
	}

	function superboss_images_embed_ratio_4_3( $image ) {
		if ( ! $image ) {
			return false;
		}

		$ratio_4_3_medium = wp_get_attachment_image_src( $image, 'ratio-4-3-medium' );
		$ratio_4_3_large  = wp_get_attachment_image_src( $image, 'ratio-4-3-large' );

		return array(
			'fallback'           => $ratio_4_3_medium[0],
			'(min-width: 500px)' => $ratio_4_3_large[0]
		);
	}

	function superboss_images_embed_ratio_3_2( $image ) {
		if ( ! $image ) {
			return false;
		}

		$ratio_3_2_medium = wp_get_attachment_image_src( $image, 'ratio-3-2-medium' );
		$ratio_3_2_large  = wp_get_attachment_image_src( $image, 'ratio-3-2-large' );

		return array(
			'fallback'           => $ratio_3_2_medium[0],
			'(min-width: 500px)' => $ratio_3_2_large[0]
		);
	}

	function superboss_images_embed_blog_post_featured( $image ) {
		if ( ! $image ) {
			return false;
		}

		$ratio_3_2_small  = wp_get_attachment_image_src( $image, 'ratio-3-2-small' );
		$square_medium    = wp_get_attachment_image_src( $image, 'square-medium' );

		return array(
			'fallback'           => $ratio_3_2_small[0],
			'(min-width: 500px)' => $square_medium[0]
		);
	}

	function superboss_images_embed_slideshow ( $image ) {
		if ( ! $image ) {
			return false;
		}

		$square_medium = wp_get_attachment_image_src( $image, 'square-medium' );
		$wide_large  = wp_get_attachment_image_src( $image, 'wide-large' );
		$wide_xlarge = wp_get_attachment_image_src( $image, 'wide-xlarge' );

		return array(
			'fallback'           => $square_medium[0],
			'(min-width: 500px)' => $wide_large[0],
			'(min-width: 980px)' => $wide_xlarge[0],
		);
	}


	/* Sharpen resized JPEG images
	============================================================================= */

	function superboss_sharpen_resized_jpeg_images( $resized_file ) {
		$image = wp_load_image( $resized_file );

		if( ! is_resource( $image ) )
			return new WP_Error( 'error_loading_image', $image, $file );

		$size = @getimagesize( $resized_file );
		if( ! $size )
			return new WP_Error( 'invalid_image', __( 'Could not read image size' ), $file );

		list( $orig_w, $orig_h, $orig_type ) = $size;

		switch( $orig_type ) {
			case IMAGETYPE_JPEG:
				$matrix = array(
					array( -1, -1, -1 ),
					array( -1, 16, -1 ),
					array( -1, -1, -1 ),
				);

				$divisor = array_sum( array_map( 'array_sum', $matrix ) );
				$offset = 0;

				imageconvolution( $image, $matrix, $divisor, $offset );
				imagejpeg( $image, $resized_file, apply_filters( 'jpeg_quality', 90, 'edit_image' ) );
				break;
			case IMAGETYPE_PNG:
				return $resized_file;
			case IMAGETYPE_GIF:
				return $resized_file;
		}

		return $resized_file;
	}

	add_filter( 'image_make_intermediate_size', 'superboss_sharpen_resized_jpeg_images', 900 );
