<?php
	/**
	 * Sidebars
	 *
	 * Functions related to sidebars.
	 *
	 * @package Superboss
	 */

	/* Register sidebar
	============================================================================= */

	function superboss_widgets_init() {
		register_sidebar( array(
			'name'          => esc_html__( 'Sidebar', 'superboss' ),
			'id'            => 'sidebar',
			'description'   => esc_html__( 'Add widgets here.', 'superboss' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		) );
	}

	add_action( 'widgets_init', 'superboss_widgets_init' );
