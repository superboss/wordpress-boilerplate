<?php
	/**
	 * Enqueue Functions
	 *
	 * Functions regarding enqueued files.
	 *
	 * @package Superboss
	 */

	/* Enqueue scripts and styles.
	============================================================================= */

	function superboss_scripts_styles() {

		// Styles
		wp_enqueue_style( 'superboss-styles', get_template_directory_uri() . '/assets/css/main.css', array(), null );

		// Scripts
		wp_enqueue_script( 'superboss-jquery', 'http://code.jquery.com/jquery-3.3.1.min.js', array(), null, true );
		wp_enqueue_script( 'superboss-scripts', get_template_directory_uri() . '/assets/js/main.js', array(), null, true );

		// Additional Scripts (delete if non-use)
		/*
		
		wp_enqueue_script( 'jquery-ui-datepicker' );

		$wp_scripts = wp_scripts();
		wp_enqueue_style('plugin_name-admin-ui-css', '//ajax.googleapis.com/ajax/libs/jqueryui/' . $wp_scripts->registered['jquery-ui-core']->ver . '/themes/smoothness/jquery-ui.min.css', false, null, false );

		*/

		/*
		
		// For AJAX security localization

		wp_localize_script( 'superboss-scripts', 'wpAJAX', array(
			'ajaxurl' => admin_url( 'admin-ajax.php' ),
			'security' => wp_create_nonce( 'ajax-action' ),
		));
		
		*/
	}

	add_action( 'wp_enqueue_scripts', 'superboss_scripts_styles' );


	/* Enqueue Modernizr file
	============================================================================= */

	function superboss_add_modernizr() {
		wp_enqueue_script( 'fonts', get_template_directory_uri() . '/assets/js/modernizr.js', array(), false, false );
	}

	add_action( 'wp_enqueue_scripts', 'superboss_add_modernizr', 10 );


	/* Enqueue fonts CSS URL from ACF Options page
	============================================================================= */
	function superboss_add_fonts() {
		$fonts_url = get_field( 'fonts_url', 'option' )
?>
<link rel="dns-prefetch" href="https://fonts.gstatic.com"> 
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin="anonymous">
<link rel="dns-prefetch" href="https://fonts.googleapis.com">
<link rel="stylesheet" href="<?php echo esc_url( $fonts_url ); ?>">
<?php
	}
	add_action( 'wp_enqueue_scripts', 'superboss_add_fonts' );

