<?php
    /**
     * Functions that affect the TinyMCE editor used by WordPress.
     *
     * @package Superboss
     */

    /* Modify TinyMCE editor to remove h1 formatting.
    ============================================================================= */

    function op_tiny_mce_remove_unused_formats( $init ) {
    	$init['block_formats'] = 'Paragraph=p;Heading 2=h2;Heading 3=h3;Heading 4=h4;Heading 5=h5;Heading 6=h6;Address=address;Pre=pre';
    	return $init;
    }

    add_filter('tiny_mce_before_init', 'op_tiny_mce_remove_unused_formats' );


    /*
     * PRE Handler
     * Solves <pre> handle in Wordpress tinyMCE editor
     * TAB support
     * TAB support to TEXT editor
     * Simple cleanup with Undo
     * Push cleanup (experimental)
     * Moves empty code chunks to beginning
    ============================================================================= */

    /*
    
    function op_TinyMCE_javascript() {

        echo '<script type="text/javascript">'."\n";
    ?>

        var ua_tinyMCE_invoked = 0;

        function ua_tinyMCE_focusbutton(){
            if(jQuery('ua-id-mce-focusbutton').get(0)) return;
            var jQueryE = jQuery('<a />').addClass('button ua-id-mce-focusbutton').css('float', 'right').on('click', function(){
                jQuery('html, body').stop().animate({ scrollTop: (jQuery('#wp-content-wrap').offset().top) - 20 }, 500);
                tinymce.activeEditor.focus();
                return false;
            }).attr('title', 'Scroll and focus the editor Toolbar in view').text('Toolbar focus').css('margin', '5px');
            jQuery('.mce-statusbar .mce-flow-layout').prepend(jQueryE);
        }

        function ua_TinyMCE_setup_callback(ed){

            if(ua_tinyMCE_invoked) return;
            ua_tinyMCE_invoked = 1;

            ed.on('init', function(e) {
                jQuery(ed.getBody()).on('click', 'pre', function() {
                    ua_TinyMCE_edit_pre(ed, this);
                    return false;
                });
                ua_tinyMCE_focusbutton();
            });
        }

        function ua_TinyMCE_helper_cleanBeginnings(str, find, replace){
            return str.replace(new RegExp(find, 'g'), replace);
        }

        function ua_TinyMCE_edit_pre(ed, obj) {
            var jQueryE = jQuery(obj); 
            var jQueryB = jQuery(ed.getBody());
            var content = jQuery('<textarea/>').html(jQueryE.html()).text();
            content = content.replace(/(<br>)/g, '');
            //content = content.replace(/   /g, '\t');
            var data = content;


            var jQueryL = jQuery('<div />').css({
                'position': 'fixed',
                'box-sizing': 'border-box',
                'background-color': 'rgba(255, 255, 255, 0.85',
                'border': '3px solid #ccc',
                'padding': '10px',
                'z-index': '9992',
                'height': 'auto',
                'width': '80%',
                'left': '50%',
                'margin-left': '-40%',
                'top': '5%'
            });

            var jQueryT = jQuery('<textarea />').keydown(function(e){

                if ( e.which != 9 ) return;
                var start = this.selectionStart;
                var end = this.selectionEnd;
                this.value = this.value.substr( 0, start ) + "\t" + this.value.substr( end );
                this.selectionStart = this.selectionEnd = start + 1;
                e.preventDefault();
                return false;

            }).attr('wrap', 'soft').css({
                'height': '98%',
                'width': '88%',
                'min-height': '300px',
                'tab-size': '3',
                'font-family': 'courier new',
                'box-sizing': 'border-box'
            });

            jQuery('#wpcontent').css('position', 'relative').append(jQueryL);
            jQueryL.append(jQueryT);
            jQueryL.append(
                jQuery('<div />').css({
                    'width': '10%',
                    'height': '100%',
                    'position': 'absolute',
                    'top': '0px',
                    'right': '10px',
                    'padding-top': '10px',
                    'box-sizing': 'border-box'
                }).append(
                    jQuery('<a />').attr('title', 'Send to element').click(function(){

                        var encodedStr = jQueryT.val().replace(/[\u00A0-\u9999<>\&]/gim, function(i) {
                            return '&#'+i.charCodeAt(0)+';';
                        });

                        jQueryE.html(encodedStr);
                        ed.focus();
                        jQueryL.remove();
                        return false;

                    }).text('Send').addClass('button button-primary').css({
                        'display': 'block',
                        'width': '100%',
                        'margin-bottom': '5px',
                        'text-align': 'center',
                        'box-sizing': 'border-box'
                    }), 

                    jQuery('<a />').attr('title', 'Cleanup').click(function(){

                        var data = jQueryT.val();
                        var original = data;
                        data = data.replace(/(\r\n|\n|\r)/gm, "\r\n");
                        var workspace = data.replace(/(\r\n|\n|\r)/gm, '');

                        if(/^\s/.test(workspace)) {
                            var search_string = workspace.replace(/^\s+|\s+$/g, '');
                            if(search_string){
                                var firstChar = search_string[0];
                                var remove = workspace.substr(0, workspace.indexOf(firstChar));
                                remove = "\r\n" + remove;
                                data = ua_TinyMCE_helper_cleanBeginnings(data, remove, "\r\n");
                            }
                            data = data.replace(/   /g, "\t");
                            data = data.replace(/^\s+|\s+$/g, '');
                        } else {
                            data = data.replace(/^\s+|\s+$/g, '');
                        }
                        if(data != original){
                            jQueryT.data('original', original);
                            if(!jQuery('#ua-TinyMCE-btt-undo').get(0)){
                            jQuery(this).after(
                                jQuery('<a />').attr('title', 'Undo').click(function(){
                                    jQueryT.val(jQueryT.data('original'));
                                    jQuery(this).remove();
                                    return false;

                                }).text('Undo').addClass('button').css({
                                    'display': 'block',
                                    'width': '100%',
                                    'margin-bottom': '5px',
                                    'text-align': 'center',
                                    'box-sizing': 'border-box'
                                }).attr('id', 'ua-TinyMCE-btt-undo')
                            );
                            }
                        }
                        data = data.replace(/   /g, "\t");
                        jQueryT.val(data);
                        return false;

                    }).text('Cleanup').addClass('button').css({
                        'display': 'block',
                        'width': '100%',
                        'margin-bottom': '5px',
                        'text-align': 'center',
                        'box-sizing': 'border-box'
                    }),

                    jQuery('<a />').attr('title', 'Close').click(function(){

                        ed.focus();
                        jQueryL.remove();
                        return false;

                    }).text('Close').addClass('button').css({
                        'display': 'block',
                        'width': '100%',
                        'margin-bottom': '5px',
                        'text-align': 'center',
                        'box-sizing': 'border-box'
                    }),

                    jQuery('<a />').attr('title', 'Remove all data').click(function(){

                        jQueryT.val('').focus();
                        return false;

                    }).text('Delete').addClass('button').css({
                        'display': 'block',
                        'width': '100%',
                        'margin-bottom': '0px',
                        'position': 'absolute',
                        'bottom': '10px',
                        'background-color': '#D54E21',
                        'color': '#fff',
                        'text-align': 'center',
                        'box-sizing': 'border-box'
                    })
                )
            );
            jQueryT.val(content).focus();
            return false;
        }

        // WP EDITOR
        jQuery(document).ready(function($){
            if($('textarea#content').get(0)){
                $('textarea#content').on('keydown', function(e){
                    if ( e.which != 9 ) return;
                    var start = this.selectionStart;
                    var end = this.selectionEnd;
                    this.value = this.value.substr( 0, start ) + "\t" + this.value.substr( end );
                    this.selectionStart = this.selectionEnd = start + 1;
                    e.preventDefault();
                    return false;
                }).css('tab-size', '3');
            }
        });

        <?php
        echo '</script>'."\n";
    }

    function op_tiny_mce_before_init(){
        add_filter( 'tiny_mce_before_init', function( $mceInit ) {
            $mceInit['setup'] = 'ua_TinyMCE_setup_callback';
            return $mceInit;
        } );

        add_action( 'before_wp_tiny_mce', 'op_TinyMCE_javascript' );
    }



    add_action( 'after_setup_theme', 'op_tiny_mce_before_init' );

    */
