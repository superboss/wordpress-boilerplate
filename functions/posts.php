<?php
	/**
	 * Functions related to posts
	 *
	 * @package Superboss
	 */

	/* Define custom excerpt word length
	============================================================================= */

	function superboss_custom_excerpt_length( $length ) {
		return 55;
	}

	add_filter( 'excerpt_length', 'superboss_custom_excerpt_length' );


	/* Define custom excerpt more text
	============================================================================= */

	function superboss_custom_excerpt_more( $more ) {
		return ' . . .';
	}

	add_filter( 'excerpt_more', 'superboss_custom_excerpt_more' );


	/* Disable tags
	============================================================================= */

	function superboss_register_post_tags() {
	    register_taxonomy( 'post_tag', array() );
	}

	// add_action( 'init', 'superboss_register_post_tags' );

	function superboss_remove_menus(){
	    remove_menu_page( 'edit-tags.php?taxonomy=post_tag' );
	}

	// add_action( 'admin_menu', 'superboss_remove_menus' );


	/* Update Default Posts Slug to read "/blog/"
	============================================================================= */

	/*

	function superboss_create_new_url_querystring() {
		add_rewrite_rule(
			'blog/([^/]*)$',
			'index.php?name=$matches[1]',
			'top'
		);
		add_rewrite_tag('%news%','([^/]*)');
	}

	add_action('init', 'superboss_create_new_url_querystring', 999 );

	function superboss_append_query_string( $url, $post, $leavename ) {
		if ( $post->post_type == 'post' ) {		
			$url = home_url( user_trailingslashit( "blog/$post->post_name" ) );
		}
		return $url;
	}

	add_filter( 'post_link', 'superboss_append_query_string', 10, 3 );
	
	*/