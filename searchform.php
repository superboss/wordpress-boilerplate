<form action="/" method="get" class="form-search">
    <label for="search" class="form-search-label offscreen">Search</label>
    <div class="form-search-input-container input-container">
    	<input type="search" name="s" id="search" value="<?php the_search_query(); ?>" class="form-search-input" placeholder="Search" />
    	<button type="submit" class="form-search-submit"><i class="material-icons">search</i><span class="offscreen">Search</span></button>
	</div>
</form>