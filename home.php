<?php
	/**
	 * The template for displaying the default Posts page. NOT the front-page template!
	 *
	 * @package Superboss
	 */

	global $wp_query;

	$pagination = superboss_posts_pagination( $wp_query );

	get_header();

	get_template_part( 'template-parts/page', 'header-posts-page' );
?>
<section class="content-main">
	<div class="container">
			<?php
				if ( have_posts() ) :
					while ( have_posts() ) : the_post();
						get_template_part( 'template-parts/content', 'post' );
					endwhile;
				else :
					get_template_part( 'template-parts/content', 'none' );
				endif;
			?>
	</div>
	<div class="container">
		<?php if ( $pagination ) : ?>
		<footer class="content-pagination">
			<?php echo $pagination; ?>
		</footer>
		<?php endif; ?>
	</div>
</section>
<?php
	get_footer();
