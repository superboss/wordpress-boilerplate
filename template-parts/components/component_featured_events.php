<?php
	/**
	 * Featured Event Posts
	 *
	 * @package Superboss
	 */

	while ( have_rows( 'component_featured_events' ) ) : the_row();
?>
<section class="homepage-events-featured">
	<?php 
		if ( have_rows( 'component_section_header' ) ) :
 			get_template_part( 'template-parts/components/component_section_header' );
		endif;
	?>
	<div class="container">
		<div class="homepage-events-featured-decoration"></div>
		<div class="event-list-items event-list-featured">
			<?php
				if ( have_rows( 'global_featured_events', 'option' ) ) :
					while ( have_rows( 'global_featured_events', 'option' ) ) : the_row(); 
						$post_object = get_sub_field( 'global_featured_events_post' ); 
						if ( $post_object ) :
							$post = $post_object; setup_postdata( $post );
							$categories = get_the_category();
							$image_id   = get_post_thumbnail_id();

							if ( ! $image_id ) :
								$image = get_field( 'placeholder_image_event', 'option' );
								$image_id = $image['ID'];
							endif;
							$responsive_image = superboss_responsive_image( superboss_images_embed_blog_post_featured( $image_id ) );

							$facility = get_field( 'event_facility' );
							$location = get_field( 'event_location' );
							$date     = get_field( 'event_start_date' );

							$classes = 'event-item event-item-featured';
						?>
						<article id="post-<?php the_ID(); ?>" <?php post_class( $classes ); ?>>
							<a href="<?php the_permalink(); ?>">
								<?php if ( $image_id ) : ?>
								<figure class="event-item-image">
									<?php echo $responsive_image; ?>
								</figure>
								<?php endif; ?>
								<div class="event-item-content">
									<?php if ( $date ) : ?>
									<div class="event-item-date">
										<?php echo date( 'F d, Y', strtotime( $date ) ); ?>
									</div>
									<?php endif; ?>
									<h3 class="event-item-title"><?php the_title(); ?></h3>
									<?php if ( $location ) : ?>
									<div class="event-item-facility"><?php echo $location; ?></div>
									<?php endif; ?>
									<div class="event-item-button">
										<span>View Event <i class="material-icons">chevron_right</i></span>
									</div>
								</div>
							</a>
						</article>
						<?php

							wp_reset_postdata();
						endif;
					endwhile;
				endif;
			?>
		</div>
	</div>
	<?php 
		if ( have_rows( 'component_section_footer' ) ) :
 			get_template_part( 'template-parts/components/component_section_footer' );
		endif;
	?>
</section>
<?php
	endwhile;	
