<?php
	/**
	 * Callout
	 *
	 * @package Superboss
	 */

	if ( function_exists('yoast_breadcrumb') && yoast_breadcrumb('','',false) ) :
		yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
	endif;
