<?php
	/**
	 * Blockquote
	 *
	 * @package Superboss
	 */

	while ( have_rows( 'component_quote' ) ) : the_row();
		$text = get_sub_field( 'component_quote_text' );
		$author = get_sub_field( 'component_quote_author' );
?>
<section class="component-quote margins-standard">
	<div class="container text-container">
		<div class="component-quote-content">
			<?php if ( $text ) : ?>
			<blockquote  class="component-quote-quote">
				&ldquo;<?php echo $text; ?>&rdquo;
			</blockquote>
			<?php endif; ?>

			<?php if ( $author && array_key_exists( 'component_quote_author_name', $author ) ) : ?>
			<div class="component-quote-author-name">
				<?php echo $author['component_quote_author_name']; ?>
			</div>
			<?php endif; ?>

			<?php if ( $author && array_key_exists( 'component_quote_author_title', $author ) ) : ?>
			<div class="component-quote-author-title">
				<?php echo $author['component_quote_author_title']; ?>
			</div>
			<?php endif; ?>
		</div>
	</div>
</section>
<?php
	endwhile;
