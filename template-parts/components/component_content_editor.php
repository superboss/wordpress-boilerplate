<?php
	/**
	 * Content Editor
	 *
	 * @package Superboss
	 */

	$content = get_sub_field( 'component_content_editor' );

	if ( $content ) :
?>
<header class="component-content-editor margins-standard">
	<div class="container text-container">
		<div class="rich-text">
			<?php echo $content; ?>
		</div>
	</div>
</header>
<?php
	endif;
