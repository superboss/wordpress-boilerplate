<?php
	/**
	 * Callout
	 *
	 * @package Superboss
	 */

	$component = get_field( 'callout' );

	if ( ! $component || ! $component['callout_heading'] ) :
		$component = get_field( 'callout', 'option' );
	endif;

	if ( $component['callout_heading'] && ( $component['callout_button'] && is_array( $component['callout_button'] ) && ! empty( $component['callout_button'] ) ) ) :
?>
<section class="component-callout">
	<div class="component-callout-container">
		<div class="component-callout-content">
			<div class="container">
				<h2 class="component-callout-heading"><?php echo $component['callout_heading']; ?></h2>
				<a href="<?php echo $component['callout_button']['url']; ?>" class="component-callout-button button-black">
					<?php echo $component['callout_button']['title']; ?>
				</a>
			</div>
		</div>
	</div>
</section>
<?php 
	endif;
