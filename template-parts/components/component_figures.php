<?php
	/**
	 * Figures
	 *
	 * @package Superboss
	 */

	if ( have_rows( 'component_figures' ) ) :
		while ( have_rows( 'component_figures' ) ) : the_row();
			$background_text = get_sub_field( 'component_figures_background_text' );
			if ( have_rows( 'component_figures_items' ) ) :
?>
<section class="component-figures">
	<div class="container">
		<div class="component-figures-row">
			<?php  while ( have_rows( 'component_figures_items' ) ) : the_row(); ?>
			<figure class="component-figures-item">
				<div class="component-figures-item-figure js-countup"><?php the_sub_field( 'component_figures_item_figure' ); ?></div>
				<figcaption class="component-figures-item-label"><?php the_sub_field( 'component_figures_item_label' ); ?></figcaption>
			</figure>
			<?php endwhile; ?>
		</div>
	</div>
	<?php if ( $background_text ) : ?>
	<figure class="component-figures-background-text">
		<?php echo $background_text; ?>
	</figure>
	<?php endif; ?>
</section>
<?php
			endif;
		endwhile;
	endif;
