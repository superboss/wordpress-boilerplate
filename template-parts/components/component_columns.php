<?php
	/**
	 * Columns
	 *
	 * @package Superboss
	 */

	while ( have_rows( 'component_columns' ) ) : the_row();
		$columns = get_sub_field( 'component_columns_columns' );
		$count = count( $columns );
?>
<section class="component-columns columns-<?php echo $count; ?>">
	<div class="container component-columns-container">
		<?php
			if ( have_rows( 'component_section_header' ) ) :
	 			get_template_part( 'template-parts/components/component_section_header' );
			endif; 
		?>
		<div class="component-columns-row">
			<?php
				while ( have_rows( 'component_columns_columns' ) ) : the_row();
					$content = get_sub_field( 'component_columns_content' );
			?>	
			<aside class="component-columns-item">
				<div class="component-columns-item-content">
					<?php if ( $content ) : ?>
					<div class="component-columns-item-title rich-text">
						<?php echo $content; ?>
					</div>
					<?php endif; ?>
				</div>
			</aside>
			<?php
				endwhile;
			?>
		</div>
	</div>
</section>
<?php
	endwhile;
