<?php
	/**
	 * Stats
	 *
	 * @package Superboss
	 */

	while ( have_rows( 'component_stats' ) ) : the_row();
?>
<section class="component-stats margins-standard">	
	<?php
		if ( have_rows( 'component_section_header' ) ) :
 			get_template_part( 'template-parts/components/component_section_header' );
		endif; 
	?>
	<div class="container component-stats-container">
		<div class="component-stats-row">
			<?php
				while ( have_rows( 'component_stats_stats' ) ) : the_row();
					$stat_icon = get_sub_field( 'component_stats_stat_icon_image' );
					$stat_type = get_sub_field( 'component_stats_stat_type' );
					$stat_text = get_sub_field( 'component_stats_stat_text' );
					$stat_desc = get_sub_field( 'component_stats_stat_description' );
			?>	
			<aside class="component-stats-item">
				<?php if ( $stat_icon ) : ?>
				<figure class="component-stats-item-icon">
					<img src="<?php echo $stat_icon['url']; ?>" alt="<?php echo $stat_icon['alt']; ?>" />
				</figure>
				<?php
					endif;

					if ( $stat_text || $stat_desc ) : ?>
				<div class="component-stats-item-content">
					<h3 class="component-stats-item-title">
						<div class="component-stats-item-text"><?php if ( $stat_type == 'money' ) : ?>$<?php endif; ?><span class="js-countup"><?php echo $stat_text; ?></span></div>
						<div class="component-stats-item-description"><?php echo $stat_desc; ?></div>
					</h3>
				</div>
				<?php endif; ?>
			</aside>
			<?php
				endwhile;
			?>
		</div>
	</div>
	<?php
		if ( have_rows( 'component_section_footer' ) ) :
 			get_template_part( 'template-parts/components/component_section_footer' );
		endif; 
	?>
</section>
<?php
	endwhile;
