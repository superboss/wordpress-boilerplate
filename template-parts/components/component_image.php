<?php
	/**
	 * Image
	 *
	 * @package Superboss
	 */

	while ( have_rows( 'component_image' ) ) : the_row();
		$image = get_sub_field( 'component_image_image' );
		$responsive_image = superboss_responsive_image( superboss_images_embed_ratio_3_2( $image['ID'] ) );
		$caption = get_sub_field( 'component_image_caption' );

		if ( $responsive_image ) :
?>
<section class="component-image margins-standard">
	<div class="container text-container">
		<div class="component-image-content">
			<figure  class="component-image-image">
				<?php echo $responsive_image; ?>
				<?php if ( $caption ) : ?>
				<figcaption class="component-image-caption">
					<?php echo $caption; ?>
				</figcaption>
				<?php endif; ?>
			</figure>
		</div>
	</div>
</section>
<?php
		endif;
	endwhile;
