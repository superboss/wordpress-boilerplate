<?php
	/**
	 * Slideshow
	 *
	 * @package Superboss
	 */

	$carousel_args = array(
		'show' => 1,
		'theme' => 'fs-light',
		'pagination' => false,
		'contained' => false
	);

	if ( have_rows( 'component_slideshow' ) ) :
		while ( have_rows( 'component_slideshow' ) ) : the_row();
			if ( have_rows( 'component_slideshow_slides' ) ) :
?>
<section class="component-slideshow margins-standard">
	<div class="container">
		<div class="component-slideshow-slides js-carousel" data-carousel-options="<?php echo superboss_json_attribute( $carousel_args ); ?>">
			<?php
				while ( have_rows( 'component_slideshow_slides' ) ) : the_row();
					$caption = get_sub_field( 'component_slideshow_slide_caption' );
					$image = get_sub_field( 'component_slideshow_slide_image' );
					$responsive_image = superboss_responsive_image( superboss_images_embed_slideshow( $image['ID'] ) );
			?>
			<div class="component-slideshow-slide">
				<?php if ( $responsive_image ) : ?>
				<figure class="component-slideshow-slide-image">
					<?php echo $responsive_image; ?>
					<figcaption class="component-slideshow-slide-content">
						<?php if ( $caption ) : ?>
						<p class="component-slideshow-slide-description"><?php echo $caption; ?></p>
						<?php endif; ?>
					</figcaption>
				</figure>
				<?php endif; ?>
			</div>
			<?php
				endwhile;
			?>
		</div>
	</div>
</section>
<?php
			endif;
		endwhile;
	endif;
