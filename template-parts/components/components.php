<?php
	/**
	 * Components Loop
	 *
	 * @package Superboss
	 */

	while ( have_rows( 'flexible_components' ) ) : the_row();
		get_template_part( 'template-parts/components/' . get_row_layout() );
	endwhile;
