<?php
	/**
	 * Section Footer
	 *
	 * @package Superboss
	 */

	while ( have_rows( 'component_section_footer' ) ) : the_row();
		if ( have_rows( 'component_section_footer_buttons' ) ) : 
?>
<footer class="component-section-footer margins-standard">
	<div class="container">
		<div class="component-section-footer-buttons">
			<?php
				while ( have_rows( 'component_section_footer_buttons' ) ) : the_row(); 
					$button = get_sub_field( 'component_section_footer_button' );
					$style  = get_sub_field( 'component_section_footer_button_style' );
			?>
			<a href="<?php echo $button['url']; ?>" class="button-<?php echo $style ?>" <?php if ( $button['target'] ) : ?>target="<?php echo $button['target']; ?>"<?php endif; ?>><?php echo $button['title']; ?></a>
			<?php endwhile; ?>
		</div>
	</div>
</footer>
<?php
		endif;
	endwhile;
