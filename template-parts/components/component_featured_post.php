<?php
	/**
	 * Featured Blog Post
	 *
	 * @package Superboss
	 */

	while ( have_rows( 'component_featured_post' ) ) : the_row();
		$post_object = get_sub_field( 'component_featured_post_post' ); 
		if ( $post_object ) :
			$post = $post_object; 
			setup_postdata( $post );

			$image_id   = get_post_thumbnail_id();

			if ( ! $image_id ) :
				$image = get_field( 'placeholder_image_event', 'option' );
				$image_id = $image['ID'];
			endif;
			$responsive_image = superboss_responsive_image( superboss_images_embed_wide( $image_id ) );
		
?>
<section class="post-featured margins-standard">
	<div class="container">
		<div class="post-featured-image-container">
			<div class="post-featured-image">
				<?php echo $responsive_image; ?>
			</div>
		</div>
		<header class="component-section-header">
			<h2 class="component-section-header-heading"><?php the_title(); ?></h2>
			<div class="component-section-header-intro rich-text">
				<?php the_excerpt(); ?>
			</div>
			<div class="post-featured-buttons">
				<a href="<?php the_permalink() ?>" class="button-primary">Read More</a>
				<a href="<?php echo site_url( 'blog' ); ?>" class="button-outlined">Go to the Blog</a>
			</div>
		</header>
	</div>
</section>
<?php
		wp_reset_postdata();
		endif;
	endwhile;
