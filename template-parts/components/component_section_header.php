<?php
	/**
	 * Section Header
	 *
	 * @package Superboss
	 */

	while ( have_rows( 'component_section_header' ) ) : the_row();
		$heading = get_sub_field( 'component_section_header_heading' );
		$intro   = get_sub_field( 'component_section_header_intro' );
?>
<header class="component-section-header margins-standard">
	<div class="container">
		<h2 class="component-section-header-heading"><?php echo $heading; ?></h2>
		<div class="component-section-header-intro rich-text">
			<?php echo $intro; ?>
		</div>
	</div>
</header>
<?php
	endwhile;
