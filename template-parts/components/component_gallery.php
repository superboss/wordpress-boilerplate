<?php
	/**
	 * Gallery
	 *
	 * @package Superboss
	 */

	$carousel_args = array(
		'theme' => 'no-theme',
		'show' => 1,
		'pagination' => false,
		'maxWidth' => '500px',
		'contained' => false,
		'controls' => array(
			'container' => '.component-gallery-controls',
			'previous' => '.component-gallery-control_previous',
			'next' => '.component-gallery-control_next'
		)
	);

	if ( have_rows( 'component_gallery' ) ) :
		while ( have_rows( 'component_gallery' ) ) : the_row();
			$images = get_sub_field( 'component_gallery_images' );
			$count = count( $images );

			if ( have_rows( 'component_gallery_images' ) ) :
?>
<section class="component-gallery margins-standard">
	<div class="container">
		<div class="component-gallery-images js-carousel-gallery" data-gallery-count="<?php echo $count; ?>" data-carousel-options="<?php echo superboss_json_attribute( $carousel_args ); ?>">
			<?php
				while ( have_rows( 'component_gallery_images' ) ) : the_row();
					$caption = get_sub_field( 'component_gallery_caption' );
					$image = get_sub_field( 'component_gallery_image' );
					$responsive_image = superboss_responsive_image( superboss_images_embed_ratio_3_2( $image['ID'] ) );
			?>
			<div class="component-gallery-item">
				<?php if ( $responsive_image ) : ?>
				<figure class="component-gallery-image">
					<?php echo $responsive_image; ?>
					<?php if ( $caption ) : ?>
					<figcaption class="component-gallery-caption">
						<?php echo $caption; ?>
					</figcaption>
					<?php endif; ?>
				</figure>
				<?php endif; ?>
			</div>
			<?php
				endwhile;
			?>
		</div>
		<div class="component-gallery-controls js-carousel-gallery-controls">
			<button class="component-gallery-control component-gallery-control_previous">
				<i class="material-icons">chevron_left</i>
				<span class="offscreen">Previous</span>
			</button>
			<div class="js-carousel-gallery-count component-gallery-count">
			</div>
			<button class="component-gallery-control component-gallery-control_next">
				<i class="material-icons">chevron_right</i>
				<span class="offscreen">Next</span>
			</button>
		</div>
	</div>
</section>
<?php
			endif;
		endwhile;
	endif;
