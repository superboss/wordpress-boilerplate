<?php
	/**
	 * Instagram feed
	 *
	 * @package Superboss
	 */

	while ( have_rows( 'component_instagram_feed' ) ) : the_row();
?>
<section class="component-instagram margins-standard">	
	<?php
		if ( have_rows( 'component_section_header' ) ) :
 			get_template_part( 'template-parts/components/component_section_header' );
		endif; 
	?>
	<div class="container component-instagram-container">
		<div class="component-instagram-row">
			<?php echo do_shortcode( '[instagram-feed num=8 cols=4 imagepadding=15 imagepaddingunit=px showfollow=false showheader=false showbutton=false]' ); ?>
		</div>
	</div>
</section>
<?php
	endwhile;
