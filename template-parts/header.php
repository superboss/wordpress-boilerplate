<?php
	/**
	 * Header
	 *
	 * @package Superboss
	 */
?>
<header class="header">
	<div class="container">
	<div class="header-navigation-container">
		<a class="header-logo" href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php bloginfo( 'name' ); ?></a>
		<nav class="main-navigation">
			<?php get_template_part( 'template-parts/navigation', 'primary' ); ?>
		</nav>
		<button tabindex="0" class="mobile-navigation-handle js-mobile-navigation-handle">Menu</button>
	</div>

	<?php
	/*
		<div class="header-search-container js-search-expand-target">
			<div class="header-search-wrapper">
				<form action="<?php echo site_url(); ?>" method="get">
				    <label for="search" class="header-search-label">Search</label>
				    <div class="header-search-input-container">
				    	<input type="search" name="s" id="search" value="<?php the_search_query(); ?>" class="header-search-input js-search-expand" />
					</div>
					<div class="header-search-submit-container">
						<button type="submit" class="header-search-submit"><i class="material-icons">search</i><span class="offscreen">Search</span></button>
					</div>
				</form>
			</div>
		</div>
	*/
	?>
</header>
