<?php
	/**
	 * 404 Page Header
	 *
	 * @package Superboss
	 */

	$classes = 'page-header page-header-basic page-header-404';
	$attr = '';

	$page_header_title = __( 'Sorry, no posts were found.', 'superboss' );
	$message_setting = get_field( 'title_404', 'option' );

	if ( $message_setting ) :
		$page_header_title = $message_setting;
	endif;

	$classes .= ' color-red';

	$image = get_field( 'image_404', 'option' );
	$image_id = $image['ID'];
	$responsive_image = superboss_responsive_image( superboss_images_embed_wide( $image_id ) );

	if ( $image_id ) :
		$classes .= ' has-image';
	endif;
?>
<header class="<?php echo esc_attr( $classes ); ?>"<?php echo $attr; ?>>
	<div class="page-header-wrapper">
		<div class="page-title-container">
			<div class="container">
				<h1 class="page-title heading-1"><?php echo esc_html( $page_header_title ); ?></h1>
			</div>
		</div>
	</div>
	<?php if ( $image_id ) : ?>
	<div class="image-wrapper">
		<div class="container">
			<figure class="featured-image">
				<?php echo $responsive_image; ?>
			</figure>
		</div>
	</div>
	<?php endif; ?>
</header>
