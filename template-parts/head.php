<?php
	/**
	 * Head Tag
	 *
	 * Partial template for the parts of the <head> tag not in wp_head()
	 *
	 * @package Superboss
	 */
?>
<head>
	<meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<?php
/*
	<link rel="icon" type="image/png" href="<?php echo esc_url( get_stylesheet_directory_uri() . '/favicon-32.png' ); ?>" sizes="32x32">
	<link rel="icon" type="image/png" href="<?php echo esc_url( get_stylesheet_directory_uri() . '/favicon-16.png' ); ?>" sizes="16x16">
*/
?>
	<?php wp_head(); ?>
</head>
