<?php
	/**
	 * Snippet for initializing Facebook API
	 *
	 * @package Superboss
	 */

	$fb_id = WPSEO_Options::get( 'fbadminapp', '' ); // get_field( 'facebook_app_id', 'option' );
?>
<script>
	var fb = <?php echo $fb_id ? 'true' : 'false'; ?>;
	<?php if ( $fb_id ) : ?>
	window.fbAsyncInit = function() {
		FB.init({
			appId            : '<?php echo $fb_id; ?>',
			autoLogAppEvents : true,
			xfbml            : true,
			version          : 'v3.0'
		});
	};
	(function(d, s, id){
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) {return;}
		js = d.createElement(s); js.id = id;
		js.src = "https://connect.facebook.net/en_US/sdk.js";
		fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
	<?php endif; ?>
</script>
