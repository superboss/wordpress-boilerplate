<?php
	/**
	 * Mobile Navigation
	 *
	 * This partial template uses the Formstone Navigation plugin
	 * for a mobile version of the primary navigation menu.
	 *
	 * @package Superboss
	 */
?>
<div tabindex="-1" class="mobile-navigation js-mobile-navigation" data-navigation-handle=".js-mobile-navigation-handle" data-navigation-content=".js-navigation-push">
	<div class="main-navigation-wrapper">
		<div class="container main-navigation-container">
			<div class="main-navigation-menu">
				<?php
				if ( has_nav_menu( 'primary_navigation' ) ) :
					wp_nav_menu( [
						'theme_location' => 'primary_navigation',
						'menu_class' => 'menu menu-main',
					] );
				endif;
				?>
			</div>
			<?php /*
			<div class="utility-navigation-menu">
				<?php get_template_part( 'template-parts/navigation', 'social-media' ); ?>
				<?php
					if ( has_nav_menu( 'utility_navigation' ) ) :
						wp_nav_menu( [
							'theme_location' => 'utility_navigation',
							'menu_class' => 'menu menu-utility',
							'container' => false,
						] );
					endif;
				?>
			</div>
			*/ ?>
		</div>
	</div>
</div>
