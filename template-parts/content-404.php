<?php
	/**
	 * Template part for displaying the 404 page content
	 *
	 * @package Superboss
	 */

	$content = false;
	$content_setting = get_field( 'content_404', 'option' );

	if ( $content_setting ) :
		$content = $content_setting;
	endif;
?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="container">
		<div class="rich-text">
			<?php
				if ( $content ) :
					echo wp_kses( $content );
				endif;
			?>
		</div>
	</div>
</article>
