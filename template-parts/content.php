<?php
	/**
	 * Template part for displaying posts
	 *
	 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
	 *
	 * @package Superboss
	 */
?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="container">
		<div class="rich-text">
			<?php the_content(); ?>
		</div>
	</div>
	<?php get_template_part( 'template-parts/components/components' ); ?>
</article>
