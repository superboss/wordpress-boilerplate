<?php
	/**
	 * Template part for displaying Post content in home.php
	 *
	 * @package Superboss
	 */

	$date       = get_the_date();
	$categories = get_the_category();
	$image_id   = get_post_thumbnail_id();

	if ( ! $image_id ) :
		$image = get_field( 'placeholder_image', 'option' );
		$image_id = $image['ID'];
	endif;
	$responsive_image = superboss_responsive_image( superboss_images_embed_ratio_3_2( $image_id ) );

	$classes = 'post-list-item';
?>
<article id="post-<?php the_ID(); ?>" <?php post_class( $classes ); ?>>
	<a href="<?php the_permalink(); ?>">
		<?php if ( $image_id ) : ?>
		<figure class="post-list-item-image">
			<?php echo $responsive_image; ?>
		</figure>
		<?php endif; ?>
		<div class="post-list-item-content">
			<h3 class="post-list-item-title"><?php the_title(); ?></h3>
			<?php if ( is_array( $categories ) && ! empty( $categories ) ) : ?>
			<div class="post-list-item-categories">
				<?php foreach ( $categories as $category ) : ?>
				<span class="post-list-item-category"><?php echo $category->name; ?></span>
				<?php endforeach; ?>
			</div>
			<?php endif; ?>
		</div>
	</a>
</article>
