<?php
	/**
	 * Page Header
	 *
	 * @package Superboss
	 */

	$classes = 'page-header page-header-basic';
	$attr = '';

	$page_header_title = get_the_title();

/*
	// If using alternate page title
	$page_header_title = get_field( 'page_heading_alternate') ? : get_the_title();
*/

/*
	// If using page intro
	$page_intro = get_field( 'page_intro' );
	if ( $page_intro ) :
		$classes .= ' has-intro';
	endif;
*/

/*
	// If using page header background color: 
	$page_header_color = get_field( 'background_color' ) ? : 'white';
	$classes .= ' color-' . $page_header_color;
*/

	$image_id = get_post_thumbnail_id();
	$responsive_image = superboss_responsive_image( superboss_images_embed_wide( $image_id ) );

	if ( $image_id ) :
		$classes .= ' has-image';
	endif;
?>
<header class="<?php echo esc_attr( $classes ); ?>"<?php echo $attr; ?>>
	<div class="page-header-wrapper">
		<div class="page-title-container">
			<div class="container">
				<?php get_template_part( 'template-parts/components/breadcrumb' ); ?>
				<h1 class="page-title heading-1"><?php echo esc_html( $page_header_title ); ?></h1>
			</div>
		</div>
		<?php if ( $page_intro ) : ?>
		<div class="page-intro-container">
			<div class="container">
				<div class="page-intro rich-text">
					<?php echo $page_intro; ?>
				</div>
			</div>
		</div>
		<?php endif; ?>
	</div>
	<?php if ( $image_id ) : ?>
	<div class="image-wrapper">
		<div class="container">
			<figure class="featured-image">
				<?php echo $responsive_image; ?>
			</figure>
		</div>
	</div>
	<?php endif; ?>
</header>
