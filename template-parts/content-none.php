<?php
	/**
	 * Template part for displaying a message that posts cannot be found
	 *
	 * @package Superboss
	 */

	$message = __( 'Sorry, no posts were found.', 'superboss' );
	$message_setting = get_field( 'no_content_found_message', 'option' );

	if ( $message_setting ) :
		$message = $message_setting;
	endif;
?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="container">
		<?php echo $message; ?>
	</div>
</article>