<?php
	/**
	 * Template part for displaying results in search pages
	 *
	 * @package Superboss
	 */
?>
<div id="post-<?php the_ID(); ?>" <?php post_class( 'search-result' ); ?>>
	<a href="<?php the_permalink(); ?>">
		<div class="search-result-content">
			<div class="search-result-type"><?php echo get_post_type(); ?></div>
			<h2 class="search-result-title"><?php the_title(); ?></h2>
			<div class="search-result-excerpt rich-text">
				<?php the_excerpt(); ?>
			</div>
		</div>
	</a>
</div>
