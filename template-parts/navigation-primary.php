<?php
	/**
	 * Primary Navigation
	 *
	 * This draws the main navigation menu for the site
	 *
	 * @package Superboss
	 */
?>
<?php
if ( has_nav_menu( 'primary_navigation' ) ) :
	wp_nav_menu( [
		'theme_location' => 'primary_navigation',
		'menu_class' => 'menu menu-main',
	] );
endif;
