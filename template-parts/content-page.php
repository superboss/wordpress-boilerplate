<?php
	/**
	 * Template part for displaying page content in page.php
	 *
	 * @package Superboss
	 */
?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="container">
		<div class="margins-standard rich-text">
			<?php the_content(); ?>
		</div>
	</div>
	<?php get_template_part( 'template-parts/components/components' ); ?>
</article>

