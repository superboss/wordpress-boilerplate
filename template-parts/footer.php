<?php
	/**
	 * Footer
	 *
	 * @package Superboss
	 */
?>
<footer class="footer content-info">
	<div class="container">
		<div class="footer-logo-container">
			<a href="/" class="footer-logo"></a>
		</div>
		<div class="footer-navigation-container">
			<nav class="footer-navigation">
				<?php
				if ( has_nav_menu( 'footer_navigation' ) ) :
					wp_nav_menu( [
						'theme_location' => 'footer_navigation',
						'menu_class' => 'menu menu-footer',
						'container' => false,
					] );
				endif;
				?>
			</nav>
		</div>
	</div>
</footer>
