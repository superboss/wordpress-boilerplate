<?php
	/**
	 * Social Media links
	 *
	 * Facebook and Twitter links come from Yoast SEO Social Media settings
	 *
	 * @package Superboss
	 */

	$social_accounts = get_option( 'wpseo_social' );
?>
<nav class="footer-social-media">
<?php
if ( is_array( $social_accounts ) && ! empty( $social_accounts ) ) :
	if ( $social_accounts['facebook_site'] ) :
?>
<a href="<?php echo esc_url( $social_accounts['facebook_site'] ); ?>" class="footer-social-media-link icon-facebook" target="_blank" title="Follow Us On Facebook">Facebook</a>
<?php
	endif;
	if ( $social_accounts['twitter_site'] ) :
?>
<a href="<?php echo esc_url( 'https://twitter.com/' . $social_accounts['twitter_site'] ); ?>" class="footer-social-media-link icon-twitter" target="_blank" title="Follow Us On Twitter">Twitter</a>
<?php
	endif;
	if ( $social_accounts['instagram_url'] ) :
?>
<a href="<?php echo esc_url( $social_accounts['instagram_url'] ); ?>" class="footer-social-media-link icon-instagram" target="_blank" title="Follow Us On Instagram">Instagram</a>
<?php
	endif;
	if ( $social_accounts['youtube_url'] ) :
?>
<a href="<?php echo esc_url( $social_accounts['youtube_url'] ); ?>" class="footer-social-media-link icon-youtube" target="_blank" title="Follow Us On YouTube">YouTube</a>
<?php
	endif;
endif;
?>
</nav>

