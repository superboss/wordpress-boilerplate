<?php
	/**
	 * Template part for displaying Post meta in a single post
	 *
	 * @package Superboss
	 */

	$post_url   = get_permalink();
	$post_title = get_the_title();

	$sharing = get_field( 'sharing', 'option' );

	// $linkedin_content = 'https://www.linkedin.com/sharing/share-offsite/?url=' . urlencode( $post_url );

	$tweet_text    = $sharing['twitter_text'];
	$tweet_content = 'https://twitter.com/share?url=' . $post_url . '&text=' . urlencode( $tweet_text . ' ' . $post_title );

	$email_subject = $sharing['email_subject_text'];
	$email_body    = $sharing['email_body_text'];
	$email_content = 'mailto:type email address here?subject=' . $email_subject . ': ' . $post_title . '&body='. $email_body . ' '. $post_title . '&#32;&#32;' . $post_url;
?>
<div class="single-post-share">
	<nav class="single-post-share-content">
		<?php /* <a href="<?php echo $linkedin_content; ?>" class="social-media-link icon-linkedin js-share-li" target="_blank" title="Share on LinkedIn">Share on LinkedIn</a> */ ?>
		<a href="<?php echo $post_url; ?>" class="social-media-link icon-facebook js-share-fb" target="_blank" title="Share on Facebook">Facebook</a>
		<a href="<?php echo $tweet_content; ?>" class="social-media-link icon-twitter js-share-tw" target="_blank" title="Share on Twitter">Twitter</a>
		<a href="<?php echo $email_content; ?>" class="social-media-link icon-email" target="_blank" title="Share with Email">Email<i class="material-icons">email</i></a>
	</nav>
</div>