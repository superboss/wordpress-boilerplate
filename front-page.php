<?php
	/**
	 * This is the page template for the Home page.
	 *
	 * @package Superboss
	 */

	get_header();

	if ( have_posts() ) :
		while ( have_posts() ) : the_post();
			get_template_part( 'template-parts/page-header', 'home' );
			get_template_part( 'template-parts/content', 'home' );
		endwhile;
	endif;

	get_footer();
