<?php
	/**
	 * The header for our theme
	 *
	 * This is the template that displays all of the <head> section and everything up until <div id="content">
	 *
	 * @package Superboss
	 */

	$page_classes = 'page';

	if ( is_front_page() ) :
		$page_classes . ' page-home';
	endif;
?>
<!doctype html>
<html class="no-js" <?php language_attributes(); ?>>
	<?php get_template_part( 'template-parts/head' ); ?>
	<body <?php body_class(); ?>>
		<a class="skip-link offscreen screen-reader-text" href="#page"><?php esc_html_e( 'Skip to content', 'superboss' ); ?></a>
		<!--[if IE]>
		<div class="alert alert-warning">
			<?php echo esc_html( 'You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.' ); ?>
		</div>
		<![endif]-->
		<div class="page-wrapper js-mobile-navigation-push">
			<?php
			if ( is_front_page() ) :
				get_template_part( 'template-parts/header', 'home' );
			else :
				get_template_part( 'template-parts/header' );
			endif;
			?>
			<main id="page" class="<?php echo esc_attr( $page_classes ); ?>" tabindex="-1" role="document">
